<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// DB::listen(function ($query)
// {
//     echo "<pre>{ $query->sql }</pre>";
// });

/**
 * Account Routes
 */
Route::resource('accounts', 'Account\AccountController', ['only' => ['index', 'destroy']]);
Route::post('accounts/{account}', 'Account\AccountController@setAccount')->name('accounts.set_account');

/**
 * Assistance Routes
 */
Route::resource('assistances', 'Assistance\AssistanceController', ['only' => ['store']]);

/**
 * Auth Routes
 */
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

/**
 * Career Routes
 */
Route::resource('careers', 'Career\CareerController', ['only' => ['index']]);
Route::resource('careers.courses', 'Career\CareerCourseController', ['except' => 'show']);
Route::get('careers/{career}', 'Career\CareerController@subjects')->name('careers.subject');
Route::get('careers/{career}/subjects/{subject}', 'Career\CareerController@groups')->name('careers.subjects.group');

/**
 * Course Routes
 */
Route::resource('courses.practices', 'Course\CoursePracticeController', ['only' => ['index']]);
Route::resource('courses.inscriptions', 'Course\CourseInscriptionController', ['only' => ['index']]);
Route::delete('courses/{course}/teachers/{teacher}', 'Course\CourseController@deleteCourse')->name('courses.teachers.delete');
Route::get('courses/{course}', 'Course\CourseInscriptionPracticeController@showCourseInscriptionPractices')->name('courses.inscriptions.practices.show');

/**
 * Group Routes
 */
Route::resource('groups', 'Group\GroupController', ['only' => ['store']]);

/**
 * Home Routes
 */
Route::get('/home', 'HomeController@index')->name('home');

/**
 * Inscription Routes
 */
Route::get('inscriptions/{inscription}', 'Inscription\InscriptionController@deliveredPractice')->name('inscriptions.practice');

/**
 * Main Routes
 */
Route::get('/', function () {
    return view('auth.login');
});

/**
 * Personal Informations Routes
 */
Route::resource('personalInformations', 'Personal_Information\Personal_InformationController');

/**
 * Period Routes
 */
Route::resource('periods', 'Period\PeriodController', ['except' => ['create', 'show', 'edit']]);

/**
 * Practice Routes
 */
Route::resource('practices', 'Practice\PracticeController');
Route::delete('practices/{practice}/registereds/{registered}', 'Practice\PracticeController@deletePractice')->name('practices.registereds.delete');

/**
 * Subject Routes
 */
Route::get('subjects/{subject}/careers/{career}', 'Subject\SubjectController@periods')->name('subjects.careers.period');

/**
 * User Routes
 */
Route::resource('users', 'User\UserController');
Route::put('users/{user}', 'User\UserController@updatePassword')->name('users.password');
Route::name('verify')->get('users/verify/{token}', 'User\UserController@verify');