<?php
use League\Csv\Reader;

function getDataFromCsv($filename, $keys, $delimiter = "\t"){
    $csv = Reader::createFromPath(base_path().'/database/seeds/csv/' . $filename);
    $csv->setDelimiter($delimiter);
    return $result = $csv->fetchAssoc($keys);
}