<?php

namespace App;

use App\Course;
use App\Account;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'career_id';

    protected $fillable = [
    	'career_id',
    	'career_name',
    ];

    public function accounts()
    {
    	return $this->hasMany(Account::class, 'career_id');
    }

    public function courses()
    {
    	return $this->hasMany(Course::class, 'career_id');
    }

    public function subjects(){
        return $this->hasMany(Subject::class, 'career_id')->orderBy('subject_name');
    }
}
