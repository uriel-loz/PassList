<?php

namespace App;

use App\Course;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'group_id';

    protected $fillable = [
    	'group_id',
    ];

    public function group()
    {
    	return $this->hasMany(Course::class, 'group_id');
    }
}
