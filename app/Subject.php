<?php

namespace App;

use App\Course;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'subject_id';

    protected $fillable = [
        'subject_id',
        'subject_code',
        'subject_name',
        'career_id',
    ];

    public function career(){
        return $this->belongsTo(Career::class, 'career_id');
    }

    public function courses()
    {
    	return $this->hasMany(Course::class, 'subject_id');
    }

    public function generatePk(){
        return $this->subject_id = $this->subject_code . $this->subject_name . $this->career_id;
    }
}