<?php

namespace App;

use App\Group;
use App\Career;
use App\Period;
use App\Subject;
use App\Registered;
use App\Inscription;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
   	public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'course_id';

    protected $fillable = [
    	'course_id',
    	'career_id',
    	'period_id',
    	'subject_id',
    	'group_id',
    ];

    public function careers()
    {
        return $this->belongsTo(Career::class, 'career_id');
    }

    public function periods()
    {
        return $this->belongsTo(Period::class, 'period_id');
    }

    public function subjects()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function groups()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function registereds()
    {
        return $this->hasMany(Registered::class, 'course_id');
    }

    public function inscriptions()
    {
        return $this->hasMany(Inscription::class, 'course_id');
    }

    public function generatePK()
    {
        $this->course_id = $this->career_id . $this->period_id . $this->subject_id . $this->group_id;
        
        return $this->course_id;
    }
}