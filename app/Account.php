<?php

namespace App;

use App\Role;
use App\User;
use App\Career;
use App\Record;
use App\Student;
use App\Teacher;
use App\Registered;
use App\Inscription;
use App\Personal_Information;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'account_id';

    protected $fillable = [
    	'account_id',
    	'career_id',
    	'user_id',
    ];

    public function careers()
    {
    	return $this->belongsTo(Career::class, 'career_id');
    }

    public function execUser()
    {
    	return $this->belongsTo(Record::class, 'exec_user_id', 'account_id');
    }

    public function receUser()
    {
    	return $this->belongsTo(Record::class, 'rece_user_id', 'account_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'account_rol', 'account_id', 'role_id')
        ->withTimestamps();
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function generatePk(){
        $this->account_id = $this->career_id . $this->user_id;
        return $this->account_id;
    }

    public function setRoleTable(array $data)
    {
        if (isset($data['student'])) {
            $user = User::findOrFail($data['user_id']);
            $student = new Student([
                'user_id' => $data['user_id']
            ]);
            $user->student()->save($student);
            return $user;
        } else {
            $user = User::findOrFail($data['user_id']);
            $teacher = new Teacher([
                'user_id' => $data['user_id']
            ]);
            $user->teacher()->save($teacher);
            return $user;
        }  
    }

    public function setRole(array $data){
        if (isset($data['student'])) {
            $account = $this->roles()->attach(Role::first());
            $account = $this->setRoleTable($data);
            return $account;
        } else {
            $account = $this->roles()->attach(Role::all()->last());
            $account = $this->setRoleTable($data);
            return $account;
        }
    }

    public function setCourse(array $data)
    {
        $student = Student::findOrFail($data['user_id']);
        
        $course = new Course(['career_id' => $data['career_id'], 'period_id' => $data['period_id'], 'subject_id' => $data['subject_id'], 'group_id' => $data['group_id']]);
        $course->generatePK();

        $inscription = new Inscription(['course_id' => $course->course_id, 'user_id' => $student->user_id]);
        $inscription->generatePk();

        $student->courses()->attach($course->course_id, ['inscription_id' => $inscription->inscription_id, 'user_id' => $student->user_id]);

        return $student;
    }

    public function denyRequest()
    {
        $this->delete();
        $personal_information = Personal_Information::findOrFail($this->user_id);
        $personal_information->delete();
        $user = User::findOrFail($this->user_id);
        $user->delete();
    }
}
