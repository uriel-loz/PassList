<?php

namespace App;

use App\Account;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	const USER_ADMIN = '1';
	const USER_COMMON = '0';

    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'role_id';

    protected $fillable = [
    	'role_id',
    	'role_name',
    	'description',
    ];

    public function userType()
    {
    	return $this->role_id == Role::USER_COMMON;
    }

    public function accounts()
    {
        return $this->belongsToMany(Account::class, 'account_rol', 'role_id', 'account_id')->withTimestamps();
    }
}
