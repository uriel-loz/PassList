<?php

namespace App;

use App\Registered;
use App\Inscription;
use Illuminate\Database\Eloquent\Model;

class Practice extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'practice_id';

    protected $fillable = [
    	'practice_id',
    	'practice_name',
    	'description',
    	'value',
    	'practice_date',
    ];

    public function inscriptions()
    {
        return $this->belongsToMany(Inscription::class, 'inscription_practice', 'practice_id', 'inscription_id')
                ->withPivot('practice_date')->withTimestamps();
    }

    public function registered()
    {
        return $this->belongsToMany(Registered::class, 'practice_registered', 'practice_id', 'registered_id')
                    ->withTimestamps();
    }

    public function generatePk()
    {
        $this->practice_id = $this->practice_name . $this->practice_date;

        return $this->practice_id;
    }
}
