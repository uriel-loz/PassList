<?php

namespace App;

use App\User;
use App\Course;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'user_id';

    protected $fillable = [
    	'user_id',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'registereds', 'user_id', 'course_id')
                ->withPivot('registered_id')
                ->withTimestamps();
    }
}
