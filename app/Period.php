<?php

namespace App;

use App\Subject;
use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'period_id';

    protected $fillable = [
    	'period_id',
    	'period_start_date',
    	'period_end_date',
    ];

    public function subjects()
    {
        return $this->belongsToMany(Subject::class, 'courses', 'period_id', 'subject_id')
                ->withPivot('course_id', 'career_id', 'group_id')
                ->withTimestamps();
    }
}
