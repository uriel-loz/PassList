<?php

namespace App\Mail;

use App\User;
use App\Personal_Information;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $personalInformation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Personal_Information $personalInformation)
    {
        $user = User::findOrFail($personalInformation->user_id);

        $this->user = $user;
        $this->personalInformation = $personalInformation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.welcome')->subject('Confirmacion de correo electronico');
    }
}
