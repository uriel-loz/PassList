<?php

namespace App\Providers;

use App\Career;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Mail\UserCreated;
use App\Personal_Information;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::share('careers', $careers = Career::all());

        Personal_Information::created(function ($personalInformation){
           Mail::to($personalInformation->email)->send(new UserCreated($personalInformation)); 
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
