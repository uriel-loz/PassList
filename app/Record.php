<?php

namespace App;

use App\Account;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    public $incrementing = false;

    protected $fillable = [
    	'timestamp',
    	'exec_user_id',
        'rece_user_id',
        'movement_type',
        'description',
    ];

    public function execUser()
    {
    	return $this->belongsTo(Account::class, 'exec_user_id', 'account_id');
    }

    public function receUser()
    {
    	return $this->belongsTo(Account::class, 'rece_user_id', 'account_id');
    }
}
