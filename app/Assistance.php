<?php

namespace App;

use App\Inscription;
use Illuminate\Database\Eloquent\Model;

class Assistance extends Model
{
    public $inscrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'assistance_date',
        'assistance',
        'inscription_id',
    ];

    public function inscriptions()
    {
        return $this->belongsTo(Inscription::class, 'inscription_id');
    }
}
