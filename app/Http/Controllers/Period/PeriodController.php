<?php

namespace App\Http\Controllers\Period;


use Response;
use Validator;
use App\Period;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class PeriodController extends Controller
{

    function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periods = Period::all();

        return view('periods.show-period', compact('periods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'period_id' => 'required|size:6|unique:periods,period_id',
            'period_start_date' => 'required',
            'period_end_date' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $period = new Period();
            $period->period_id = $request->period_id;
            $period->period_start_date = $request->period_start_date;
            $period->period_end_date = $request->period_end_date;
            $period->save();

            return response()->json($period);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Period $period)
    {
        $rules = [
            'period_start_date' => 'required',
            'period_end_date' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $period->period_id = $request->period_id;
            $period->period_start_date = $request->period_start_date;
            $period->period_end_date = $request->period_end_date;
            $period->save();

            return response()->json($period);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function destroy(Period $period)
    {
        $period->delete();
        return response()->json($period);
    }
}
