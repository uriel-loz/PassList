<?php

namespace App\Http\Controllers\Subject;

use Response;
use App\Career;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Course;


class SubjectController extends Controller
{
   function __construct()
   {
       $this->middleware('roles:Maestro');
   }

   /**
     * Retrieve groups from subject and career
     *
     * @param  \App\Career  $career
     */
   public function periods(Subject $subject, Career $career)
   {
       $periods = Course::select('period_id')
                    ->where('career_id', $career->career_id)
                    ->where('subject_id', $subject->subject_id)
                    ->distinct()
                    ->get();

        return Response::json($periods);
   }
}
