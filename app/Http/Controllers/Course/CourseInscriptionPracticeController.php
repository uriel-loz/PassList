<?php

namespace App\Http\Controllers\Course;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CourseInscriptionPracticeController extends Controller
{
    public function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCourseInscriptionPractices(Course $course)
    {
        $career = $course->careers;
        $period = $course->periods;
        $group = $course->groups;
        $subject = $course->subjects;

        $course_practices = $course->with('inscriptions.students.user.personalInformation', 'inscriptions.practices', 'registereds.practices')->get()->where('course_id', '=', $course->course_id);

        return view('inscriptions.show-inscriptions-practice', compact('career', 'period', 'group', 'subject', 'course_practices'));
    }
}
