<?php

namespace App\Http\Controllers\Course;

use App\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;
use App\Registered;

class CourseController extends Controller
{

    function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    /**
     * Remove the specified course from storage.
     *
     * @param  \App\Course  $course
     * @return \App\Teacher $teacher
     */
    public function deleteCourse(Course $course, Teacher $teacher)
    {
        $registered = new Registered(['course_id' => $course->course_id, 'user_id' => $teacher->user_id]);
        $registered->generatePk();

        $teacher->courses()->detach($course->course_id, ['registered_id' => $registered->registered_id, 'user_id' => $teacher->user_id]);

        $course->delete();
        return response()->json($course);
    }
}
