<?php

namespace App\Http\Controllers\Course;

use App\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CourseInscriptionController extends Controller
{

    public function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course)
    {
        $inscriptions = $course->inscriptions;
        $career =  $course->careers;
        $subject = $course->subjects;
        $group = $course->groups;
        $period = $course->periods;

        //Get information through relationships, in this case of course towards personal information models.
        // $personal_informations = $course->inscriptions()->with('students.user.personalInformation')->get();

        $assistance_dates = DB::table('assistances')
                    ->join('inscriptions', 'assistances.inscription_id', '=', 'inscriptions.inscription_id')
                    ->select('assistance_date')
                    ->distinct()
                    ->where('inscriptions.course_id', '=', $course->course_id)
                    ->orderBy('assistance_date', 'desc')
                    ->get();

        $assistance_student = $course->inscriptions()->with('assistances', 'students.user.personalInformation')->get();

        $course_practices = $course->registereds()->with('practices')->get()->pluck('practices')->collapse();

        return view('courses.course-inscription', compact('career', 'subject', 'group', 'period', 'assistance_dates', 'assistance_student', 'course_practices', 'course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        //
    }
}
