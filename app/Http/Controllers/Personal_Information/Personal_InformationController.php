<?php

namespace App\Http\Controllers\Personal_Information;

use Illuminate\Http\Request;
use App\Personal_Information;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;

class Personal_InformationController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Personal_Information  $personal_Information
     * @return \Illuminate\Http\Response
     */
    public function show(Personal_Information $personal_Information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Personal_Information  $personal_Information
     * @return \Illuminate\Http\Response
     */
    public function edit(Personal_Information $personal_Information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Personal_Information  $personal_Information
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $personalInformation = Personal_Information::findOrFail($id);

        $this->authorize($personalInformation);

        $personalInformation->update($request->all());

        return back()->with('info', 'Datos del usuario actualizados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Personal_Information  $personal_Information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personal_Information $personal_Information)
    {
        //
    }
}
