<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct()
    {
        $this->middleware('roles:Maestro')->except('verify', 'updatePassword');
        $this->middleware('auth')->except('verify');
    }

    public function index()
    {
        $users = DB::table('users')
                ->join('personal__informations', 'users.user_id', '=', 'personal__informations.user_id')
                ->join('accounts', 'users.user_id', '=', 'accounts.user_id')
                ->join('careers', 'accounts.career_id', '=', 'careers.career_id')
                ->join('account_rol', 'accounts.account_id', '=', 'account_rol.account_id')
                ->join('roles', 'account_rol.role_id', '=', 'roles.role_id')
                ->join('students', 'users.user_id', '=', 'students.user_id')
                ->join('inscriptions', 'students.user_id', '=', 'inscriptions.user_id')
                ->join('courses', 'inscriptions.course_id', '=', 'courses.course_id')
                ->join('subjects', 'courses.subject_id', '=', 'subjects.subject_id')
                ->select('personal__informations.*', 'careers.career_name', 'careers.career_id', 'roles.role_name', 
                            DB::raw("GROUP_CONCAT(subjects.subject_name ORDER BY subjects.subject_name SEPARATOR ', ') as subjects"))
                ->groupBy('personal__informations.user_id')
                ->get();

        return view('users.user-show', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function verify($verifcationToken)
    {
        $user = User::where('verification_token', $verifcationToken)->firstOrFail();
        $user->verified = User::VERIFIED;
        $user->verification_token = null;

        $user->save();

        return redirect('/')->with('success', 'Tu correo ha sido verificado, por favor espera a que el profesor valide tu cuenta');
    }

    public function updatePassword(Request $request, User $user)
    {
       $rules = [
           'old_password' => 'required',
           'password' => 'required|min:6|confirmed'
       ];

       $this->validate($request, $rules);

       if (Hash::check($request->old_password, $user->password)) {
           $user->password = Hash::make($request->password);
           $user->save();
           return back()->with('info', 'La contraseña a sido cambiada');
       } else {
           return back()->with('danger', 'Error la contraseña introduccida es erronea!');
       }
    }
}
