<?php

namespace App\Http\Controllers\Practice;

use Response;
use Validator;
use App\Course;
use App\Practice;
use App\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class PracticeController extends Controller
{

    public function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'practice_name' => 'required|min:4',
            'description' => 'required|min:4',
            'value' => 'required|numeric',
            'practice_date' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
           return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $practice = new Practice();
            $practice->practice_id = $request->practice_name . $request->practice_date;
            $practice->practice_name = $request->practice_name;
            $practice->description = $request->description;
            $practice->value = $request->value;
            $practice->practice_date = $request->practice_date;

            $practice->save();
            
            $registered = new Registered(['course_id' => $request->course_id, 'user_id' => $request->user_id]);
            $registered->generatePk();

            $registered->practices()->attach($request->practice_name . $request->practice_date);

            $practices = DB::table('practice_registered')
                        ->join('practices', 'practice_registered.practice_id', '=', 'practices.practice_id')
                        ->join('registereds', 'practice_registered.registered_id', '=', 'registereds.registered_id')
                        ->select('practices.*')
                        ->where('registereds.course_id', '=', $request->course_id)
                        ->get();

            return response()->json($practices);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function show(Practice $practice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function edit(Practice $practice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Practice $practice)
    {
        $rules = [
            'practice_name' => 'required|min:4',
            'description' => 'required|min:4',
            'value' => 'required|numeric',
            'practice_date' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $practice->practice_name = $request->practice_name;
            $practice->description = $request->description;
            $practice->value = $request->value;
            $practice->practice_date = $request->practice_date;
            $practice->save();

            $practices = DB::table('practice_registered')
                        ->join('practices', 'practice_registered.practice_id', '=', 'practices.practice_id')
                        ->join('registereds', 'practice_registered.registered_id', '=', 'registereds.registered_id')
                        ->select('practices.*')
                        ->where('registereds.course_id', '=', $request->course_id)
                        ->get();

            return response()->json($practices);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Practice $practice)
    {
        
    }

    /**
     * Remove the specified practice from practices.
     *
     * @param  \App\Practice  $practice
     * @param  \App\Registered $registered
     */
    public function deletePractice(Practice $practice, Registered $registered)
    {
        $course = $registered->course_id;

        $registered->practices()->detach($practice->practice_id);
        $practice->delete();

        $practices = DB::table('practice_registered')
        ->join('practices', 'practice_registered.practice_id', '=', 'practices.practice_id')
        ->join('registereds', 'practice_registered.registered_id', '=', 'registereds.registered_id')
        ->select('practices.*')
        ->where('registereds.course_id', '=', $course)
        ->get();

        return response()->json($practices);
    }
}
