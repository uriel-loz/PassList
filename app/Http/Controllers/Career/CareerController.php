<?php

namespace App\Http\Controllers\Career;

use Response;
use App\Career;
use App\Course;
use App\Period;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CareerController extends Controller
{

    function __construct(){
        $this->middleware('roles:Maestro');
        $this->middleware('auth')->except('subjects, groups');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $careers = Career::all();

        return view('shared._navbar', compact('careers'));
    }

    /**
     * Retrieve subjects to career
     *
     * @param  \App\Career  $career
     */
    public function subjects(Career $career)
    {
        $courses = DB::table('courses')
                    ->join('subjects', 'courses.subject_id', '=', 'subjects.subject_id')
                    ->select('subjects.subject_id', 'subjects.subject_name')
                    ->where('courses.career_id', '=', $career->career_id)
                    ->distinct()
                    ->get();

        return Response::json($courses);
    }

     /**
     * Retrieve subjects to career
     *
     * @param  \App\Career  $career
     * @param  \App\Subject  $subject
     */
    public function groups(Career $career, Subject $subject)
    {
        $groups = Course::select('group_id')
                    ->where('career_id', $career->career_id)
                    ->where('subject_id', $subject->subject_id)
                    ->get();
        return Response::json($groups);
    }
}
