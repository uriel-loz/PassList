<?php

namespace App\Http\Controllers\Career;

use App\Group;
use App\Career;
use App\Course;
use App\Period;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;
use App\Registered;

class CareerCourseController extends Controller
{

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware('roles:Maestro');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Career $career)
    {
        $subjects = $career->courses()->with('subjects')->get();

        return view('careers.career-show', compact('career', 'subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Career $career)
    {
        $subjects = $career->subjects;
        $periods = Period::all();

        return view('courses.course-create', compact('career', 'subjects', 'periods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Career $career)
    {
        $rules = [
            'period_id' => 'required',
            'subject_id' => 'required',
            'group_id' => 'required|size:4'
        ];

        $this->validate($request, $rules);

       Course::create([
           'course_id' => $career->career_id . $request->input('period_id') . $request->input('subject_id') . $request->input('group_id'),
           'career_id' => $career->career_id,
           'period_id' => $request->input('period_id'),
           'subject_id' => $request->input('subject_id'),
           'group_id' => $request->input('group_id')
       ]);

       $course = new Course(['career_id' => $career->career_id, 'period_id' => $request->input('period_id'), 'subject_id' => $request->input('subject_id'), 'group_id' => $request->input('group_id')]);
       $course->generatePK();

       if (auth()->check()) {
           $teacher = Teacher::findorFail(auth()->user()->user_id);

           $registered_id = $course->course_id . $teacher->user_id;
           $teacher->courses()->attach($course->course_id, ['registered_id' => $registered_id, 'user_id' => $teacher->user_id]);
       }

       //redirect
       return redirect()->route('careers.courses.index', $career->career_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function edit(Career $career, Course $course)
    {
        $subject = $course->subjects;
        $periods = Period::all();
        
        return view('courses.course-edit', compact('career', 'subject','periods', 'course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Career $career, Course $course)
    {
        $rules = [
            'period_id' => 'required',
            'group_id' => 'required|size:4',
            'justification' => 'required|min:10'
        ];

        $this->validate($request, $rules);

        $course->course_id = $course->generatePK($career->career_id . $request->input('period_id') . $course->subject_id . $request->input('group_id'));
        $course->career_id = $career->career_id;
        $course->period_id = $request->input('period_id');
        $course->subject_id = $course->subject_id;
        $course->group_id = $request->input('group_id');

        $course->save();
 
        return redirect()->route('careers.courses.index', $career->career_id);
    }
}
