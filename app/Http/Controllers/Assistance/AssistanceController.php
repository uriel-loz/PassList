<?php

namespace App\Http\Controllers\Assistance;

use App\Assistance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AssistanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assistance = new Assistance();
        $assistance->assistance_date = $request->assistance_date;
        $assistance->assistance = $request->assistance;
        $assistance->inscription_id = $request->inscription_id;
        $assistance->save();

        return response()->json($assistance);
    }
}
