<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

     /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $careers = Career::all();

        return view('auth.register', compact('careers'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_id' => 'required|string|size:9',
            'name' => 'required|string|max:75',
            'last_name' => 'required|string|max:75',
            'last_mother_name' => 'required|string|max:75',
            'email' => 'required|string|email|max:150|unique:personal__informations',
            'phone' => 'required|alpha_num|unique:personal__informations',
            'career_id' => 'required',
            'password' => 'required|min:6|confirmed',
            'gender' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::setAccountsData($data);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if ($user->verified === User::NO_VERIFIED) {
            Auth::logout();
            return redirect('/login')->with('success', 'Necesitas verificar tu correo, hemos enviado un enlace a tu correo para verificarlo');
        }

        return redirect($this->redirectPath());
    }
}
