<?php

namespace App\Http\Controllers\Inscription;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Inscription;

class InscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('roles:Maestro');
    }

    public function deliveredPractice(Inscription $inscription)
    {
        if (isset($_GET)) {
            $data = $_GET;
        }

        $inscription->practices()->attach($data['practice_id'], ['practice_date' => $data['practice_date']]);

        return response()->json($inscription);
    }
}
