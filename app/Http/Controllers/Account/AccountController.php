<?php

namespace App\Http\Controllers\Account;

use Response;
use Validator;
use App\User;
use App\Period;
use App\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class AccountController extends Controller
{

    function __construct()
    {
        $this->middleware('roles:Maestro');
        $this->middleware('auth')->except('setAccount');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usersRequests = DB::table('users')
                            ->join('personal__informations', 'users.user_id', '=', 'personal__informations.user_id')
                            ->join('accounts', 'users.user_id', '=', 'accounts.user_id')
                            ->join('careers', 'accounts.career_id', 'careers.career_id')
                                ->whereNotExists(function($query){
                                    $query->select('account_rol.account_id')
                                            ->from('account_rol')
                                            ->whereRaw('account_rol.account_id = accounts.account_id');
                                })
                            ->select('personal__informations.*', 'careers.career_name', 'careers.career_id')
                            ->get();

        return view('users.user-requests', compact('usersRequests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->denyRequest();
        
        return redirect()->route('accounts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @param  \Illuminate\Http\Request  $request
     */
    public function setAccount(Account $account)
    {
        if (isset($_POST['student'] )) {
            $data = $_POST;
            $user = User::findOrFail($data['user_id']);
            if (!$user->countRoles()) {
                $account->setRole($data);
            }
            $account->setCourse($data);
            return response()->json($account);
        }else {
            $data = $_POST;
            $account->setRole($data);
            return response()->json($account);
        }
    }
}
