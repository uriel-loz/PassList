<?php

namespace App\Http\Controllers\Group;

use Response;
use Validator;
use App\Group;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GroupController extends Controller
{

    function __construct()
    {
        $this->middleware('roles:Maestro');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = [
           'group_id' => 'required|size:4|alpha_num'
       ];

       $validator = Validator::make(Input::all(), $rules);

       if ($validator->fails()) {
           return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
       } else {
          $group = new Group();
          $group->group_id = $request->group_id;
          $group->save();

          return response()->json($group);
       }
    }
}
