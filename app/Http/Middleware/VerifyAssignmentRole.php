<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyAssignmentRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->countRoles()) {
            return $next($request);
        }

        Auth::logout();
        return redirect('/')->with('warning', 'Tu cuenta aún no tiene un rol asignado, por favor espera a que se le asigne uno.');
    }
}
