<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class UsersVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->verified == User::VERIFIED) {
            return $next($request);
        }

        Auth::logout();
        return redirect('/')->with('warning', 'Tu cuenta no a sido verificada por favor revisa tu correo');
    }
}
