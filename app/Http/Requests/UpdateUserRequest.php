<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'name' => 'required',
            'last_name' => 'required',
            'last_mother_name' => 'required',
            'email' => 'required|unique:personal__informations,email,'.$this->route('personalInformation').',user_id',
            'phone' => 'required|unique:personal__informations,phone,'.$this->route('personalInformation').',user_id',
            'gender' => 'required'
        ];
    }
}
