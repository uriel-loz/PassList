<?php

namespace App;

use App\User;
use App\Course;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'user_id';

    protected $fillable = [
    	'user_id',
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'inscriptions', 'user_id', 'course_id')
                ->withPivot('inscription_id')
                ->withTimestamps();
    }
}
