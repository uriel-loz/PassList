<?php

namespace App;

use App\Course;
use App\Teacher;
use App\Practice;
use Illuminate\Database\Eloquent\Model;

class Registered extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'registered_id';

    protected $fillable = [
        'registered_id',
        'course_id',
        'user_id',
    ];

    public function courses()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function practices()
    {
        return $this->belongsToMany(Practice::class, 'practice_registered', 'registered_id', 'practice_id')
                ->withTimestamps();
    }

    public function teachers()
    {
        return $this->belongsTo(Teacher::class, 'user_id');
    } 
    
    public function generatePk()
    {
        $this->registered_id = $this->course_id . $this->user_id;

        return $this->registered_id;
    }
}
