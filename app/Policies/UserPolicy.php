<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Personal_Information;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
       if ($user->isAdmin()) {
           return true;
       }
    }

    public function update(User $authUser, Personal_Information $personalInformation)
    {
        return $authUser->user_id === $personalInformation->user_id;
    }
}
