<?php

namespace App;

use App\Course;
use App\Student;
use App\Practice;
use App\Assistance;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'inscription_id';

    protected $fillable = [
        'inscription_id',
        'course_id',
        'user_id',
    ];

    public function courses()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    public function students()
    {
        return $this->belongsTo(Student::class, 'user_id');
    }

    public function practices()
    {
        return $this->belongsToMany(Practice::class, 'inscription_practice', 'inscription_id', 'practice_id')
                ->withPivot('practice_date')->withTimestamps();
    }

    public function assistances()
    {
        return $this->hasMany(Assistance::class, 'inscription_id');
    }

    public function generatePk()
    {
        $this->inscription_id = $this->course_id . $this->user_id;
        return $this->inscription_id;
    }
}
