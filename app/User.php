<?php

namespace App;

use App\Career;
use App\Account;
use App\Student;
use App\Teacher;
use App\Personal_Information;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const VERIFIED = '1';
    const NO_VERIFIED = '0';

    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'password',
        'remember_token',
        'verified',
        'verification_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'verification_token',
    ];

    public function userVerified()
    {
        return $this->verified = User::VERIFIED;
    }

    public static function generateVerificationToken()
    {
        return str_random(40);
    }

    public function student()
    {
        return $this->hasOne(Student::class, 'user_id');
    }

    public function teacher()
    {
        return $this->hasOne(Teacher::class, 'user_id');
    }

    public function personalInformation()
    {
        return $this->hasOne(Personal_Information::class, 'user_id');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class, 'user_id');
    }

    public function hasRoles(array $roles)
    {
        foreach ($this->accounts()->with('roles')->get()->pluck('roles') as $userRoles) {
            return $userRoles->pluck('role_name')->intersect($roles)->count();
        }

        // foreach ($this->accounts()->with('roles')->get()->pluck('roles') as $userRoles) {
        //     foreach ($userRoles as  $userRole) {
        //         foreach ($roles as $role) {
        //             if ($userRole->role_name === $role) {
        //                 return true;
        //             }
        //         }
        //     }
        // }

        return false;
    }

    public function isAdmin()
    {
        return $this->hasRoles(['Maestro']);
    }

    public function countRoles()
    {
        return $this->accounts()->with('roles')->get()->pluck('roles')->collapse()->count();
    }

    public static function setAccountsData($data)
    {
        $career = Career::findOrFail($data['career_id']);

        $personalInformation = new Personal_Information([
            'user_id' => $data['user_id'],
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'last_mother_name' => $data['last_mother_name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
        ]);

        $user = User::create([
            'user_id' => $data['user_id'],
            'password' => bcrypt($data['password']),
            'verified' => User::NO_VERIFIED,
            'verification_token' => User::generateVerificationToken(),
        ]);
        $user->personalInformation()->save($personalInformation);

        $account = new Account([
            'career_id' => $career->career_id,
            'user_id' => $user->user_id,
        ]);
        $account->generatePk();
        $user->accounts()->save($account);

        return $user;
    }
}
