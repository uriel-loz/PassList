<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->char('account_id', 12)->primary();
            $table->char('career_id', 3)->index();
            $table->char('user_id', 9)->index();
            $table->timestamps();

            $table->foreign('career_id')
            ->references('career_id')->on('careers');
            $table->foreign('user_id')
            ->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
