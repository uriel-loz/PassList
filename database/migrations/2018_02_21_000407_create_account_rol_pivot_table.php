<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountRolPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_rol', function (Blueprint $table) {
            $table->char('account_id', 12);
            $table->char('role_id', 1);
            $table->timestamps();

            $table->foreign('account_id')
            ->references('account_id')->on('accounts');
            $table->foreign('role_id')
            ->references('role_id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_rol');
    }
}
