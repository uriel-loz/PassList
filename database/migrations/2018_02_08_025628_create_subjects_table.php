<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->char('subject_id', 85)->primary();
            $table->char('subject_code', 4);
            $table->char('subject_name', 75)->index();
            $table->char('career_id', 3)->index();
            $table->timestamps();

            $table->foreign('career_id')
            ->references('career_id')->on('careers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
