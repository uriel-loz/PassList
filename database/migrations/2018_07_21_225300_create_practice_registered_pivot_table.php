<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePracticeRegisteredPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('practice_registered', function (Blueprint $table) {
            $table->char('registered_id', 107);
            $table->string('practice_id', 90);
            $table->timestamps();

            $table->foreign('practice_id')
            ->references('practice_id')->on('practices')->onDelete('cascade');
            $table->foreign('registered_id')
            ->references('registered_id')->on('registereds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('practice_registered');
    }
}
