<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->dateTime('timestamp')->nullable()->index();
            $table->char('exec_user_id', 12)->nullable();
            $table->char('rece_user_id', 12)->index();
            $table->string('movement_type', 50)->index();
            $table->string('description', 255)->nullable();

            $table->foreign('exec_user_id')
            ->references('account_id')->on('accounts');
            $table->foreign('rece_user_id')
            ->references('account_id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
