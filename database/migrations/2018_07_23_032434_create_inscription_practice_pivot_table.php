<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscriptionPracticePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscription_practice', function (Blueprint $table) {
            $table->char('inscription_id', 107);
            $table->string('practice_id', 90);
            $table->date('practice_date');
            $table->timestamps();

            $table->foreign('inscription_id')
            ->references('inscription_id')->on('inscriptions')->onDelete('cascade');
            $table->foreign('practice_id')
            ->references('practice_id')->on('practices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscription_practice');
    }
}
