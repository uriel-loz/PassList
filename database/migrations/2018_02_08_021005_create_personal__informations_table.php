<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal__informations', function (Blueprint $table) {
            $table->char('user_id', 9)->primary();
            $table->string('name', 75)->index();
            $table->string('last_name', 75)->index();
            $table->string('last_mother_name', 75);
            $table->string('email', 150)->unique();
            $table->string('phone', 20)->unique();
            $table->char('gender', 1);
            $table->timestamps();

            $table->foreign('user_id')
            ->references('user_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal__informations');
    }
}
