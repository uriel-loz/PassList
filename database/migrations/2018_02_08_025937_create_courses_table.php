<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->char('course_id', 98)->primary();
            $table->char('career_id', 3)->index();
            $table->char('period_id', 6)->index();
            $table->char('subject_id', 85)->index();
            $table->char('group_id', 4)->index();
            $table->timestamps();

            $table->foreign('career_id')
            ->references('career_id')->on('careers');
            $table->foreign('period_id')
            ->references('period_id')->on('periods');
            $table->foreign('subject_id')
            ->references('subject_id')->on('subjects');
            $table->foreign('group_id')
            ->references('group_id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
