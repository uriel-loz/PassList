<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisteredsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registereds', function (Blueprint $table) {
           $table->char('registered_id', 107)->primary();
           $table->char('course_id', 98);
           $table->char('user_id', 9);
           $table->timestamps();

           $table->foreign('course_id')
           ->references('course_id')->on('courses');
           $table->foreign('user_id')
           ->references('user_id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registereds');
    }
}
