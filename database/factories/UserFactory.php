<?php

use App\Role;
use App\User;
use App\Group;
use App\Period;
use App\Career;
use App\Subject;
use App\Teacher;
use App\Student;
use App\Practice;
use App\Personal_Information;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//User Factory
$factory->define(User::class, function (Faker $faker) {
    static $password;

    $users = ['311242872', '412929020', '312283838', '315839282', '415836320', '318282828', '412929022', '412919020', '413929420', '418929020', '412923420', '412929520', '412920020', '412909020', '412969020', '412925420', '412922320', '412329020'];
    $check = [User::VERIFIED, User::NO_VERIFIED];

    return [
    	'user_id'=>$faker->unique()->randomElement($users),
        'password'=>$password ?: $password = bcrypt('secret'),
        'remember_token'=>str_random(10),
        'verified'=>$verified = $faker->randomElement($check),
        'verification_token'=>$verified === User::VERIFIED ? null : User::generateVerificationToken()
    ];
});

//Personal_Information Factory
$factory->define(Personal_Information::class, function (Faker $faker) {

    $genders = ['F', 'M'];

    return [
        'name'=>$faker->firstName,
        'last_name'=>$faker->lastName,
        'last_mother_name'=>$faker->lastName,
        'email'=>$faker->unique()->freeEmail,
        'phone'=>$faker->tollFreePhoneNumber,
        'gender'=>$faker->randomElement($genders)
    ];
});

//Role Factory
$factory->define(Role::class, function (Faker $faker) {
    $roles = [Role::USER_ADMIN, Role::USER_COMMON];
    return [
        'role_id'=>$roleId = $faker->unique()->randomElement($roles),
        'role_name'=>$roleId == Role::USER_ADMIN ? 'Maestro' : 'Alumno',
        'description'=>$faker->text
    ];
});

//Group Factory
$factory->define(Group::class, function (Faker $faker) {
    
    return [
        'group_id'=>$faker->numerify('####')
    ];
});

//Period Factory
$factory->define(Period::class, function (Faker $faker) {
    
    $year = '2018';
    $numberPeriod = '2';

    $start = $faker->numerify($year . '-02-04');
    $end = $faker->numerify($year . '-06-10');

    return [
        'period_id'=>$faker->numerify($year . '-' . $numberPeriod),
        'period_start_date'=>$start,
        'period_end_date'=>$end
    ];
});

//Practice Factory
$factory->define(Practice::class, function (Faker $faker) {
    
    $practices = ['Tipos de dato2012-01-02', 'Matrices2018-01-01', 'Numeros Romanos2017-01-05', 'Cartas2018-03-03', 'Cambio de estructuras2012-02-03'];

    return [
        'practice_id'=>$faker->unique()->randomElement($practices),
        'practice_name'=>$faker->word,
        'description'=>$faker->text,
        'value'=>$faker->numerify('0#.##'),
        'practice_date'=>$faker->date
    ];
});

//Career Factory
// $factory->define(Career::class, function (Faker $faker) {
//     $careers = ['Informatica', 'Ing. Telecomunicaciones', 'Ing. Mecanico Electrica'];

//     return [
//         'career_id'=>$faker->unique()->numerify('###'),
//         'career_name'=>$faker->unique()->randomElement($careers)
//     ];
// });

//Subject Factory
// $factory->define(Subject::class, function (Faker $faker) {

//     return [
//         'subject_id'=>$faker->unique()->word,
//         'subject_code'=>$faker->unique()->numerify('####'),
//         'subject_name'=>$faker->word
//     ];
// });