<?php

use App\Group;
use App\Career;
use App\Period;
use App\Subject;
use Illuminate\Database\Seeder;

class PeriodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $career = Career::first();
        $group = Group::first();
        $subjects = Subject::all();

        Period::truncate();
        Period::flushEventListeners();
        
        $period = factory(Period::class)->create();

        $subjects->each(function ($subject) use ($career, $group, $period){
            $course_id = $career->career_id . $period->period_id . $subject->subject_id . $group->group_id;
            $period->subjects()->attach($subject->subject_id, ['course_id' => $course_id, 'career_id' => $career->career_id, 'period_id' => $period->period_id, 'group_id' => $group->group_id]);
        });
    }
}
