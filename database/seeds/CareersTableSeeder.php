<?php

use App\Career;
use Illuminate\Database\Seeder;

class CareersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Career::truncate();
        Career::flushEventListeners();

        $keys = ['career_id', 'career_name'];
        $results = getDataFromCsv('careers.csv', $keys);
        foreach($results as $row){
            $result = new Career($row);
            $result->save();
        }
    }
}