<?php

use App\Role;
use App\User;
use App\Career;
use App\Account;
use App\Student;
use App\Teacher;
use App\Personal_Information;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        User::flushEventListeners();
        Personal_Information::flushEventListeners();

        $numberUser = 18;
        $career = Career::first();

        factory(User::class, $numberUser)->create()->each(
            function ($user) use ($career){
                $personal_information = factory(Personal_Information::class)->make();
                $user->personalInformation()->save($personal_information);
                //Set user's personal infomations

                $accounts = new Account(['career_id' => $career->career_id, 'user_id' => $user->user_id]);
                $accounts->generatePk();
                $user->accounts()->save($accounts);
                //Set accounts for users

                $shufle = rand(1, 2);
                //random role assignment
                if ($shufle == 1) {
                    $roles = Role::pluck('role_id')->first();
                } else {
                    $roles = Role::pluck('role_id')->last();
                }
                $accounts->roles()->attach($roles);
                //Set role for users

                $accountsRoles = $accounts->roles;

                $accountsRoles->each(function ($accounRole) use ($user){
                    if ($accounRole->role_id == '0') {
                        $student = new Student(['user_id' => $user->user_id]);
                        $user->student()->save($student);
                    } else {
                        $teachers = new Teacher(['user_id' => $user->user_id]);
                        $user->teacher()->save($teachers);
                    }
                });
            }
        );
    }
}