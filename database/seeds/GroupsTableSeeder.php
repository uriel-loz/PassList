<?php

use App\Group;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberGroups = 8;

        Group::truncate();
        Group::flushEventListeners();

        factory(Group::class, $numberGroups)->create();
    }
}
