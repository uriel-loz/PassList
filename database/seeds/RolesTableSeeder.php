<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberRoles = 2;

        Role::truncate();
        Role::flushEventListeners();

        factory(Role::class, $numberRoles)->create();

    }
}
