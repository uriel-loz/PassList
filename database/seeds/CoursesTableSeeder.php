<?php

use App\Course;
use App\Student;
use App\Subject;
use App\Teacher;
use App\Practice;
use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        Course::flushEventListeners();

        $courses = Course::all();
        $teacher = Teacher::first();
        $student = Student::first();
        $practices = Practice::all();

        $courses->each(function ($course) use ($teacher, $student, $practices){
            $practices->each(function ($practice) use ($teacher, $course){
                $teacher->courses()->attach($course->course_id, ['practice_id' => $practice->practice_id, 'user_id' => $teacher->user_id]);
            });

            $practices->each(function ($practice) use ($student, $course){
                $student->courses()->attach($course->course_id, ['user_id' => $student->user_id, 'practice_id' => $practice->practice_id, 'practice_date' => $practice->practice_date]);
            });
        });
    }
}
