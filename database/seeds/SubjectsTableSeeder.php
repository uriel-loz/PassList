<?php

use App\Subject;
use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $numberSubjects = 20;

        Subject::truncate();
        Subject::flushEventListeners();

        $keys = ['subject_id', 'subject_code', 'subject_name', 'career_id'];
        $results = getDataFromCsv('subjects.csv', $keys);
        foreach($results as $row){
            $result = new Subject($row);
            $result->save();
        }

        // $career = App\Career::first();
        
        // factory(App\Subject::class, 20)->create()->each(
        //     function ($subject) use ($career) {
        //         $subjects = new App\Subject(['career_id'=>$career->career_id]);
        //         $subject->career()->save($subjects);
        // });
    }
}