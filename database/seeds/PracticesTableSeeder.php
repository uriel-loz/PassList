<?php

use App\Practice;
use Illuminate\Database\Seeder;

class PracticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberPractices = 5;

        Practice::truncate();
        Practice::flushEventListeners();

        factory(Practice::class, $numberPractices)->create();

    }
}
