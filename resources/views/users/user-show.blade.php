@extends('layouts.app')

@section('section')
    Usuarios Registrados
@endsection

@section('content')
    <div class="form-group">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Tabla de usuarios registrados
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr role="row">
                                <th colspan="8">Datos de cuenta</th>
                                <th colspan="2">Opciones</th>
                            </tr>
                            <tr>
                                <th>Número de cuenta</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Teléfono</th>
                                <th>Carrera</th>
                                <th>Asignaturas</th>
                                <th>Fecha y hora de registro</th>
                                <th>Rol</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($users as $user)
                           <tr class="item{{ $user->user_id }}">
                                <td>{{ $user->user_id }}</td>
                                <td>{{ $user->name . ' ' . $user->last_name . ' ' . $user->last_mother_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->career_name }}</td>
                                <td>{{ $user->subjects }}</td>
                                <td>{{ date('d/m/Y H:m:s', strtotime($user->created_at))}}</td>
                                <td>{{ $user->role_name }}</td>
                                <td>
                                    <button class="btn btn-outline-info nav-link pass-assign" data-toggle="modal" type="button" data-target="#userAssign" data-user-id="{{ $user->user_id }}" data-career-id="{{ $user->career_id }}"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Reasignar otro grupo</button>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Número de cuenta</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Teléfono</th>
                                <th>Carrera</th>
                                <th>Asignaturas</th>
                                <th>Fecha y hora de registro</th>
                                <th>Rol</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('users.modals.reassign-group')
    @push('crud')
        @include('users.ajax.retrieve_js')
    @endpush
@endsection