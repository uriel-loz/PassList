@include('shared._root-route')

{{-- function to retrieve data of courses from database --}}
<script type="text/javascript">
    $(function () {  
        
        var $subjectsAssign = $('#subjectsAssign'); //variable of the select assign
        var $groupsAssign = $('#groupsAssign'); //variable of the select groups
        var $periodsAssign = $('#periodsAssign'); //variable of the select periods
        var $modalAssign = $('#userAssign'); //variable of the modal
        var $assignStudent = $('.assign-student'); //variable of button student
        var $assignTeacher = $('.assign-teacher'); //variable of button teacher
        var $reassingStudent = $('.reassign-student');//variable of button reassing course student
        

        $(document).on('click', '.pass-assign', function () {         
            $('#careersAssign').val($(this).data('career-id'));
            $('#usersAssign').val($(this).data('user-id'));
            
            var $user_id = $('#usersAssign').val();
            var $career_id = $('#careersAssign').val();
            
            $.ajax({
                type: 'GET',
                url: url + '/' + 'careers' + '/' + $career_id,
                success: function (results) {
                    var e = window.event;
                    if (e) {
                        $subjectsAssign.empty();
                        $subjectsAssign.prepend("<option>-- Selecciona una asignatura --</option>");
                        for (var i = 0; i < results.length; i++) {
                            $subjectsAssign.append("<option value='" + results[i].subject_id + "'>" + results[i].subject_name + "</option>");
                        }
                    }
                },
            });
            user_id = $user_id;
            career_id = $career_id;
        });

        $($subjectsAssign).on('change', function () {  
           $subject_id = $subjectsAssign.val();
        
           $.ajax({
                type: 'GET',
                url: url + '/' + 'careers' + '/' + career_id + '/' + 'subjects' + '/' + $subject_id,
                success: function (results) {  
                   var e = window.event;
                   if (e) {
                        $groupsAssign.removeAttr('disabled');
                        $groupsAssign.empty();
                        $groupsAssign.prepend("<option>-- Selecciona un grupo --</option>");
                        for (var i = 0; i < results.length; i++) {
                           $groupsAssign.append("<option value='" + results[i].group_id + "'>" + results[i].group_id + "</option>");
                        }
                   }   
                },
            });

            subject_id = $subject_id;
        });

        $($groupsAssign).on('change', function () {  
            $.ajax({
                type: 'GET',
                url: url + '/' + 'subjects' + '/' + subject_id + '/' + 'careers' + '/' + career_id,
                success: function (results) { 
                    var e = window.event;
                    if (e) {
                        $periodsAssign.removeAttr('disabled');
                        $periodsAssign.empty();
                        for (var i = 0; i < results.length; i++) {
                            $periodsAssign.append("<option value='" + results[i].period_id + "'>" + results[i].period_id + "</option>");
                        }
                    }
                },
            });    
        });

        $assignStudent.on('click', function () {  
            $.ajax({
                type: 'POST',
                url: url + '/' + 'accounts' + '/' + career_id + user_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'user_id': user_id,
                    'career_id': career_id,
                    'subject_id': subject_id,
                    'group_id': $groupsAssign.val(),
                    'period_id': $periodsAssign.val(),
                    'student': '0'
                },
                success: function (data) {  
                    toastr.success('Se ha asigno a un usuario con Rol Estudiante', 'Asignación Exitosa!', {timeOut: 4000});
                    $('.item' + data['user_id']).remove();
                    $groupsAssign.empty();
                    $periodsAssign.empty();
                },
            });
        });

        $assignTeacher.on('click', function () { 
            $career_id = $(this).data('career-id');
            $user_id = $(this).data('user-id');
            
            $.ajax({
                type: 'POST',
                url: url + '/' + 'accounts' + '/' + $career_id + $user_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'user_id': $user_id,
                },
                success: function (data) {  
                    toastr.success('Se ha asigno a un usuario con Rol Maestro', 'Asignación Exitosa!', {timeOut: 4000});
                    $('.item' + $user_id).remove();
                },
            });
        });

        $reassingStudent.on('click', function () {  
            $.ajax({
                type: 'POST',
                url: url + '/' + 'accounts' + '/' + career_id + user_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'user_id': user_id,
                    'career_id': career_id,
                    'subject_id': subject_id,
                    'group_id': $groupsAssign.val(),
                    'period_id': $periodsAssign.val(),
                    'student': '0'
                },
                success: function (data) {  
                    toastr.success('Se ha asigno un nuevo curso', 'Asignación Exitosa!', {timeOut: 4000});
                    $groupsAssign.empty();
                    $periodsAssign.empty();
                },
            });
        });

        $modalAssign.on('click', '.cancel', function () {  
            $groupsAssign.empty();
            $periodsAssign.empty();
        });
    });
</script>