<!-- Modal for reassign groups to users-->
<div class="modal fade" id="userAssign" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Asignar Grupo</h5>
                <button class="close cancel" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form role="form">
                        <input type="text" id="usersAssign" class="hidden">
                        <input type="text" id="careersAssign" class="hidden">
                        <div class="form-group">
                          <label for="">Asignatura:</label>
                          <select class="form-control" name="subjects_id" id="subjectsAssign" required>
                            <option value="">-- Selecciona una asignatura --</option>
                          </select>
                        </div>
                        <div class="form-group">
                            <label for="">Grupo:</label>
                            <select class="form-control" name="group_id" id="groupsAssign" disabled required>
                                <option value=""></option>  
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Periodo:</label>
                            <select class="form-control" name="period_id" id="periodsAssign" disabled required> 
                                <option value=""></option> 
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-success reassign-student" data-dismiss="modal">Asignar</button>
            </div>
        </div>
    </div>
</div>