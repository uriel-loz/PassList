@extends('layouts.app')

@section('section')
    Peticiones de Registro
@endsection

@section('content')
    <div class="form-group">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Tabla de peticiones de usuario para tener acceso al sistema.
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr role="row" class="text-center">
                                <th colspan="6">Datos del usuario</th>
                                <th colspan="3">Opciones</th>
                            </tr>
                            <tr role="row">
                                <th>Numero de cuenta</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Telefono</th>
                                <th>Carrera</th>
                                <th>Fecha y hora de Registro</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usersRequests as $user)
                                <tr class="item{{ $user->user_id }}">
                                    <td>{{ $user->user_id }}</td>
                                    <td>{{ $user->name . ' ' . $user->last_name . ' ' . $user->last_mother_name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->career_name }}</td>
                                    <td>{{ date('d/m/Y H:m:s', strtotime($user->created_at)) }}</td>
                                    <td>
                                        <button class="btn btn-outline-success nav-link pass-assign" data-toggle="modal" type="button" data-target="#userAssign" data-user-id="{{ $user->user_id }}" data-career-id="{{ $user->career_id }}"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Asignar grupo</button>
                                    </td>
                                    <td>
                                        <button class="btn btn-outline-info assign-teacher" data-user-id="{{ $user->user_id }}" data-career-id="{{ $user->career_id }}" type="submit"><i class="fa fa-coffee" aria-hidden="true"></i> Asignar como profesor</button>    
                                    </td>
                                    <td>
                                        <form method="POST" action="{{ route('accounts.destroy', $user->career_id . $user->user_id) }}">
                                            @csrf
                                            {!! method_field('DELETE') !!}
                                            
                                            <button class="btn btn-outline-danger" type="submit"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Negar petición</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Numero de cuenta</th>
                                <th>Nombre</th>
                                <th>Correo electronico</th>
                                <th>Telefono</th>
                                <th>Carrera</th>
                                <th>Hora y fecha de Registro</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('users.modals.assign-group')

    @push('crud')
        @include('users.ajax.retrieve_js')
    @endpush

@endsection