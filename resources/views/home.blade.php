@extends('layouts.app')

@section('section')
    Perfil
@endsection

@section('content')

    @if (session()->has('info'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session('info') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session()->has('danger'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('danger') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <ul class="nav nav-pills mb-3 col-sm-9 offset-sm-3 col-md-12 col-lg-9 offset-lg-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a href="#pills-profile" class="nav-link active btn-outline-info" data-toggle="pill" role="tab" aria-controls="profile" aria-selected="true" id="pills-profile-tab"><i class="fa fa-user-o" aria-hidden="true"></i> Perfil</a>
        </li>
        <li class="nav-item">
            <a href="#pills-profile-update" class="nav-link btn-outline-info" data-toggle="pill" role="tab" aria-controls="profile-update" aria-selected="false" id="pills-profile-update-tab"><i class="fa fa-refresh" aria-hidden="true"></i> Actualizar Información</a>
        </li>
        <li class="nav-item disabled">
            <a href="#pills-change-password" class="nav-link btn-outline-info" data-toggle="pill" role="tab" aria-controls="change-password" aria-selected="false" id="pills-change-password-tab"><i class="fa fa-unlock" aria-hidden="true"></i> Cambiar Contraseña</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pill-profile-tab" id="pills-profile">
            <form action="">
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Número de Cuenta:</label>
                        <div class="col-sm-6">
                        <p class="form-control-plaintext">{{ auth()->user()->user_id }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Nombre:</label>
                        <div class="col-sm-6">  
                            <p class="form-control-plaintext">{{ auth()->user()->personalInformation->name }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Apellido Paterno:</label>
                        <div class="col-sm-6">
                        <p class="form-control-plaintext">{{ auth()->user()->personalInformation->last_name }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Apellido Materno:</label>
                        <div class="col-sm-6">
                            <p class="form-control-plaintext">{{ auth()->user()->personalInformation->last_mother_name }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Email</label>
                        <div class="col-sm-6">
                            <p class="form-control-plaintext">{{ auth()->user()->personalInformation->email }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Teléfono</label>
                        <div class="col-sm-6">
                            <p class="form-control-plaintext">{{ auth()->user()->personalInformation->phone }}</p>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-4 col-form-label">Género:</label>
                        <div class="col-sm-6">
                            <p class="form-control-plaintext">{{ auth()->user()->personalInformation->gender == 'M' ? 'Masculino' : 'Femenino' }}</p>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="tab-pane fade" role="tabpanel" aria-labelledby="pills-profile-update-tab" id="pills-profile-update">
            <form method="POST"  action="{{ route('personalInformations.update', auth()->user()->personalInformation->user_id) }}">
                @csrf
                {!! method_field('PUT') !!}

                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Número de Cuenta:</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="tel" class="form-control" name="user_id" value="{{ auth()->user()->user_id }}" disabled>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Nombre:</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ auth()->user()->personalInformation->name }}">
                            @if ($errors->has('name'))
                                <span class="error">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Apellido Paterno:</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" class="form-control" name="last_name" value="{{ auth()->user()->personalInformation->last_name }}">
                            @if ($errors->has('last_name'))
                                <span class="error">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Apellido Materno:</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="text" class="form-control" name="last_mother_name" value="{{ auth()->user()->personalInformation->last_mother_name }}">
                            @if ($errors->has('last_mother_name'))
                                <span class="error">
                                    <strong>{{ $errors->first('last_mother_name') }}</strong>
                                </span>
                            @endif
                        </div
                    ></div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Email</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ auth()->user()->personalInformation->email }}">
                            @if ($errors->has('email'))
                                <span class="error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Teléfono</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="tel" class="form-control" name="phone" value="{{ auth()->user()->personalInformation->phone }}">
                            @if ($errors->has('phone'))
                                <span class="error">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-12 col-md-1 offset-md-4 col-lg-2 offset-lg-2 col-form-label">Género:</label>
                    <div class="form-check form-check-inline">
                        <input type="radio" class="form-check-input" value="M" name="gender" {{ auth()->user()->personalInformation->gender == 'M' ? 'checked' : ''}}>
                        <label for="" class="form-check-label">Masculino</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input type="radio" class="form-check-input" value="F" name="gender" {{ auth()->user()->personalInformation->gender == 'F' ? 'checked' : '' }}>
                        <label for="" class="form-check-label">Femenino</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <button type="submit" class="btn btn-outline-success col-sm-2 col-md-2 offset-md-5 col-lg-3 offset-lg-4"><i class="fa fa-refresh" aria-hidden="true"></i> Actualizar Perfil</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="tab-pane fade" role="tabpanel" aria-labelledby="pills-change-password-tab" id="pills-change-password">
            <form method="POST" action="{{ route('users.password', auth()->user()->user_id) }}">

                @csrf
                {!! method_field('PUT') !!}

                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Contraseña Actual:</label>
                        <div class="col-sm-8 col-md-6">
                            <input type="password" class="form-control password" name="old_password" required>
                            @if ($errors->has('old_password'))
                                <span class="error">
                                    <strong>{{ $errors->first('old_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Nueva Contraseña:</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="password" class="form-control password" name="password" required>
                            @if ($errors->has('password'))
                                <span class="error">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <label for="" class="col-sm-2 offset-sm-2 col-form-label">Verificar Contraseña:</label>
                        <div class="col-sm-12 col-md-6">
                            <input type="password" class="form-control password" name="password_confirmation" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="form-check offset-md-5 ">
                            <input class="form-check-input" type="checkbox" id="show" value=""> Mostrar Contraseñas
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <button type="submit" class="col-sm-12 col-md-3 offset-md-4 btn  btn-outline-danger">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Cambiar Contraseña
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>       
@endsection