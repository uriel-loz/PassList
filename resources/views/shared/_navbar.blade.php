<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand login_effect" href="{{ route('home') }}">{{ auth()->user()->personalInformation->fullName() }}</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Profile">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="fa fa-fw fa-user-circle" aria-hidden="true"></i>
                <span class="nav-link-text">Perfil</span>
            </a>
        </li>
        @if (auth()->user()->hasRoles(['Maestro']))
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Careers">
                <a class="nav-link nav-link-collapse collapsed" id="careers" data-toggle="collapse" href="#collapseCareers" data-parent="#exampleAccordion">
                    <i class="fa fa-fw fa-outdent" aria-hidden="true"></i>
                    <span class="nav-link-text">Carreras</span>
                </a>
                <div id="careers-list">
                    <ul class="sidenav-second-level collapse" id="collapseCareers">
                        @foreach ($careers as $career)
                        <li>
                            <a href="{{ route('careers.courses.index', $career->career_id) }}">{{ $career->career_name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Users Request">
                <a href="{{ route('accounts.index') }}" class="nav-link">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                    <span class="nav-link-text">Peticiones de registro</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Students">
                <a href="{{ route('users.index') }}" class="nav-link">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span class="nav-link-text">Estudiantes</span>
                </a>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Periods">
                <a href="{{ route('periods.index') }}" class="nav-link">
                    <i class="fa fa-calendar-plus-o" aria-hidden="true"></i>
                    <span class="nav-link-text">Periodos</span>
                </a>
            </li>
        @endif
    </ul>
    <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-angle-double-left fa-fw"></i>
        </a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Messages
            <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
            <i class="fa fa-fw fa-circle"></i>
            </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">New Messages:</h6>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <strong>David Miller</strong>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <strong>Jane Smith</strong>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <strong>John Doe</strong>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="#">View all messages</a>
        </div>
        </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-bell"></i>
            <span class="d-lg-none">Alerts
            <span class="badge badge-pill badge-warning">6 New</span>
            </span>
            <span class="indicator text-warning d-none d-lg-block">
            <i class="fa fa-fw fa-circle"></i>
            </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">New Alerts:</h6>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <span class="text-success">
                <strong>
                <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
            </span>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <span class="text-danger">
                <strong>
                <i class="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
            </span>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
            <span class="text-success">
                <strong>
                <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
            </span>
            <span class="small float-right text-muted">11:21 AM</span>
            <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="#">View all alerts</a>
        </div>
        </li>
        <li class="nav-item">
            <a name="" id="" class="btn btn-outline-danger nav-link" href="{{ route('logout') }}" role="button"><i class="fa fa-fw fa-sign-out"></i> Cerrar Sesión</a>
        </li>
    </ul>
    </div>
</nav>