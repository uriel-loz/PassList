@include('shared._root-route')
{{-- AJAX CRUD operations --}}
<script type="text/javascript">
    $(function () {
    //Add a new practice
        $('.modal-footer').on('click', '.add', function () {
            $.ajax({
                type : 'POST',
                url : url + '/' + 'practices',
                data : {
                    '_token' : $('input[name=_token]').val(),
                    'title' : $('#title_add').val(),
                    'description' : $('#description_add').val(),
                    'value' : $('#value_add').val(),
                    'date' : $('#date_add').val()
                },
                success : function (data) {
                    $('.errorTitle').addClass('d-none');
                    $('.errorContent').addClass('d-none');

                    if ((data.errors)) {
                        setTimeout(function () {
                            $('#addModal').modal('show');
                            toastr.error('Validation error!', 'Error Alert', {timeOut: 2000});
                        }, 500);

                        if (data.errors.title) {
                            $('.errorTitle').removeClass('d-none');
                            $('.errorTitle').text(data.errors.title);
                        }

                        if (data.errors.description) {
                            $('.errorContent').removeClass('d-none');
                            $('.errorContent').text(data.errors.description);
                        }

                        if (data.errors.value) {
                            $('.errorTitle').removeClass('d-none');
                            $('.errorTitle').text(data.errors.value);
                        }

                        if (data.errors.date) {
                            $('.errorContent').removeClass('d-none');
                            $('.errorContent').text(data.errors.date);
                        }
                    } else {
                        toastr.success('Se ha agregado una nueva practica!', 'Registro Exitoso!', {timeOut: 2000});
                    }
                }, 
            });
        });
    });
</script>