<div class="container-fluid">
<!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">PassList</a>
        </li>
        <li class="breadcrumb-item active">
            @yield('section', 'Main')
        </li>
        @yield('show')
        @yield('option')
        @yield('data')
    </ol>
    <div class="container-fluid">
        @yield('content')
    </div>
</div>