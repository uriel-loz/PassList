<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Manage Groups PassList</title>
    <!-- Bootstrap core CSS, Sb-admin and FontAwesome -->
    <link href="/css/app.css" rel="stylesheet">
    <!-- Page level plugin CSS -->
    <link rel="stylesheet" href="/css/dataTables.bootstrap4.css">
  </head>