@include('shared._head')

  @if (auth()->check())
    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
      <!-- navbar -->
      @include('shared._navbar')
      <!--content-->
      <div class="content-wrapper">
        @include('shared._sections')
        <!-- /.content-wrapper-->
        @include('shared._footer')
        <!-- Scroll to Top Button-->
        @include('shared._effect')
      </div> 
  @endif

@include('shared._scripts')