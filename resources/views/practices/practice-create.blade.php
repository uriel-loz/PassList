<!-- Modal for create a new practice-->
<div class="modal fade" id="createPractice" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Nueva practica</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <form role="form">
                <div class="form-group row">
                    <label for="" class="col-sm-2">Tema:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="practice_name" id="practice-name">
                        <p class="error-practice-name text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="">Descripción: </label>
                    <textarea name="description" rows="3" class="form-control" id="practice-description"></textarea>
                    <p class="error-practice-description text-center alert alert-danger hidden col-12"></p>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="">Valor:</label>
                        <input type="number" class="form-control" name="value" step="0.1" id="practice-value">
                        <p class="error-practice-value text-center alert alert-danger hidden"></p>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Fecha de entrega:</label>
                        <input type="date" class="form-control" name="practice_date" id="practice-date">
                        <p class="error-practice-date text-center alert alert-danger hidden"></p>
                    </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-primary create" data-dismiss="modal">Registrar</button>
            </div>
        </div>
    </div>
</div>