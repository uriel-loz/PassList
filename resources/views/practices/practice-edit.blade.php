<!-- Modal for register a practice-->
<div class="modal fade" id="editPractice" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">Practicas existentes</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                <form role="form">
                    <div class="form-group row">
                        <label for="" class="col-sm-2">Tema:</label>
                        <div class="col-sm-10">
                            <input type="text" name="practice_name" id="edit-practice-name" class="form-control" disabled>
                            <p class="error-practice-name text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="">Description: </label>
                        <textarea name="description" rows="3" class="form-control" id="edit-practice-description"></textarea>
                        <p class="error-practice-description text-center alert alert-danger hidden col-12"></p>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Valor:</label>
                            <input type="number" class="form-control" name="value" step="0.1" id="edit-practice-value">
                            <p class="error-practice-value text-center alert alert-danger hidden"></p>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Fecha de entrega:</label>
                            <input type="date" class="form-control" name="practiceDate" id="edit-practice-date">
                            <p class="error-practice-date text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger cancel" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-outline-primary edit" data-dismiss="modal">Registrar</button>
                </div>
            </div>
        </div>
    </div>