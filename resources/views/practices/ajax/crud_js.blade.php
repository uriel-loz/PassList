@include('shared._root-route')

<script type="text/javascript">
    $(function () {  
        var $newPractice = $('.btn-create-practice');
        var $createPractice = $('.create');
        var $practiceName = $('#practice-name');
        var $practiceDescription = $('#practice-description');
        var $practiceValue = $('#practice-value');
        var $praticeDate = $('#practice-date');
        var $errorName = $('.error-practice-name');
        var $errorDescription = $('.error-practice-description');
        var $errorValue = $('.error-practice-value');
        var $errorDate = $('.error-practice-date');
        var $modalCreate = $('#createPractice');
        var $modalEdit = $('#editPractice');
        var $modalDelete = $('#deletePractice');
        var $tableBody = $('#table-body');
        var $editPractice = $('.btn-edit-practice');

        $newPractice.on('click', function () {              
            var $course_id = $(this).data('course-id');
            var $user_id = $(this).data('user-id');
            var $career_name = $(this).data('career-name');

            user_id = $user_id;
            course_id = $course_id
            career_name = $career_name
        });

        $createPractice.on('click', function () {  
            $.ajax({
                type: 'POST',
                url: url + '/' + 'practices',
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'practice_name': $practiceName.val(),
                    'description': $practiceDescription.val(),
                    'value': $practiceValue.val(),
                    'practice_date': $praticeDate.val(),
                    'course_id': course_id,
                    'user_id': user_id
                },
                success: function (data) {  
                    $errorName.addClass('hidden');
                    $errorDescription.addClass('hidden');
                    $errorValue.addClass('hidden');
                    $errorDate.addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {  
                            $modalCreate.modal('show');
                            toastr.error('Error de Validación', 'Alerta de Error!', {timeOut: 4000});
                        }, 500);
                        
                        if (data.errors.practice_name) {
                            $errorName.removeClass('hidden');
                            $errorName.text(data.errors.practice_name);
                        }

                        if (data.errors.description) {
                            $errorDescription.removeClass('hidden');
                            $errorDescription.text(data.errors.description);
                        }

                        if (data.errors.value) {
                            $errorValue.removeClass('hidden');
                            $errorValue.text(data.errors.value);
                        }

                        if (data.errors.practice_date) {
                            $errorDate.removeClass('hidden');
                            $errorDate.text(data.errors.practice_date);
                        }
                    } else {
                        toastr.success('Practica creada!', 'Registro Exitoso', {timeOut: 4000});
                        $tableBody.html(""); 
                        
                        for (var i = 0; i < data.length; i++) {
                            $tableBody.append("<tr><td>" + data[i].practice_name + "</td><td>" + data[i].description + "</td><td>" + data[i].value + "</td><td>" + convertDateFormat(data[i].practice_date) + "</td><td><button type='button' class='btn btn-outline-info nav-link btn-edit-practice' data-toggle='modal' data-target='#editPractice' data-practice-id='" + data[i].practice_id + "' data-practice-name='" + data[i].practice_name + "' data-practice-description='" + data[i].description + "' data-practice-value='" + data[i].value + "' data-practice-date='" + data[i].practice_date + "' data-course-id='" + course_id + "' data-user-id='" + user_id + "'><i class='fa fa-file-text-o' aria-hidden='true'></i> Editar Practica</button></td><td><button type='button' class='btn btn-outline-danger nav-link btn-delete-practice' data-toggle='modal' data-target='#deletePractice' data-practice-id='" + data[i].practice_id + "' data-practice-name='" + data[i].practice_name + "' data-user-id='" + user_id + "' data-course-id='" + course_id + "'><i class='fa fa-chain-broken' aria-hidden='true' ></i> Eliminar Practica</button></td></tr>");
                        }
                    }
                }
            });
            
            $practiceName.val('');
            $practiceDescription.val('');
            $practiceValue.html("");
            $praticeDate.html("");
        });

        $(document).on('click', '.btn-edit-practice', function () {  
           var $editName = $('#edit-practice-name');
           var $editDescription = $('#edit-practice-description');
           var $editValue = $('#edit-practice-value');
           var $editDate = $('#edit-practice-date');
           var $careerName = $(this).data('career-name');
           var $course_id = $(this).data('course-id');
           var $user_id = $(this).data('user-id');
           var $practice_id = $(this).data('practice-id')

           $editName.val($(this).data('practice-name'));
           $editDescription.val($(this).data('practice-description'));
           $editValue.val($(this).data('practice-value'));
           $editDate.val($(this).data('practice-date'));

           practice_id = $practice_id;
           career_name = $careerName;
           course_id = $course_id;
           user_id = $user_id;
        });

        $modalEdit.on('click', '.edit', function () {  
            $.ajax({
                type: 'PUT',
                url: url + '/' + 'practices' + '/' + practice_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'practice_id': practice_id,
                    'practice_name': $('#edit-practice-name').val(),
                    'description': $('#edit-practice-description').val(),
                    'value': $('#edit-practice-value').val(),
                    'practice_date': $('#edit-practice-date').val(),
                    'course_id': course_id
                },
                success: function (data) {  
                    $errorName.addClass('hidden');
                    $errorDescription.addClass('hidden');
                    $errorValue.addClass('hidden');
                    $errorDate.addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {  
                            $modalEdit.modal('show');
                            toastr.error('Error de Validación', 'Alerta de Error!', {timeOut: 4000});
                        }, 500);

                        if (data.errors.practice_name) {
                            $errorName.removeClass('hidden');
                            $errorName.text(data.errors.practice_name);
                        }

                        if (data.errors.description) {
                            $errorDescription.removeClass('hidden');
                            $errorDescription.text(data.errors.description);
                        }

                        if (data.errors.value) {
                            $errorValue.removeClass('hidden');
                            $errorValue.text(data.errors.value);
                        }

                        if (data.errors.practice_date) {
                            $errorDate.removeClass('hidden');
                            $errorDate.text(data.errors.practice_date);
                        }
                    } else {
                        toastr.success('Se actualizo la información de la practica', 'Registro Exitoso!', {timeOut: 4000});

                        $tableBody.html("");                   

                        for (var i = 0; i < data.length; i++) {
                            $tableBody.append("<tr><td>" + data[i].practice_name + "</td><td>" + data[i].description + "</td><td>" + data[i].value + "</td><td>" + convertDateFormat(data[i].practice_date) + "</td><td><button type='button' class='btn btn-outline-info nav-link btn-edit-practice' data-toggle='modal' data-target='#editPractice' data-practice-id='" + data[i].practice_id + "' data-practice-name='" + data[i].practice_name + "' data-practice-description='" + data[i].description + "' data-practice-value='" + data[i].value + "' data-practice-date='" + data[i].practice_date + "' data-course-id='" + course_id + "' data-user-id='" + user_id + "'><i class='fa fa-file-text-o' aria-hidden='true'></i> Editar Practica</button></td><td><button type='button' class='btn btn-outline-danger nav-link btn-delete-practice' data-toggle='modal' data-target='#deletePractice' data-practice-id='" + data[i].practice_id + "' data-practice-name='" + data[i].practice_name + "' data-user-id='" + user_id + "' data-course-id='" + course_id + "'><i class='fa fa-chain-broken' aria-hidden='true' ></i> Eliminar Practica</button></td></tr>");
                        }
                    }   
                },
            });
        });

        $(document).on('click', '.btn-delete-practice', function () { 
            var $deleteName = $('#delete-practice-name');
            var $practiceId = $(this).data('practice-id');
            var $userId = $(this).data('user-id');
            var $courseId = $(this).data('course-id');

            $deleteName.val($(this).data('practice-name'));
            
            practice_id = $practiceId;
            course_id = $courseId;
            user_id = $userId;
        });

        $modalDelete.on('click', '.delete', function () {  
            $.ajax({
                type: 'DELETE',
                url: url + '/' + 'practices' + '/' + practice_id + '/' + 'registereds' + '/' + course_id + user_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'justification': $('#justification').val(),
                },
                success: function (data) {  
                    toastr.success('Practica Eliminada', 'Eliminación Exitosa', {timeOut: 4000});
                    
                    $tableBody.html("");                   

                    for (var i = 0; i < data.length; i++) {
                        $tableBody.append("<tr><td>" + data[i].practice_name + "</td><td>" + data[i].description + "</td><td>" + data[i].value + "</td><td>" + convertDateFormat(data[i].practice_date) + "</td><td><button type='button' class='btn btn-outline-info nav-link btn-edit-practice' data-toggle='modal' data-target='#editPractice' data-practice-id='" + data[i].practice_id + "' data-practice-name='" + data[i].practice_name + "' data-practice-description='" + data[i].description + "' data-practice-value='" + data[i].value + "' data-practice-date='" + data[i].practice_date + "' data-course-id='" + course_id + "' data-user-id='" + user_id + "'><i class='fa fa-file-text-o' aria-hidden='true'></i> Editar Practica</button></td><td><button type='button' class='btn btn-outline-danger nav-link btn-delete-practice' data-toggle='modal' data-target='#deletePractice' data-practice-id='" + data[i].practice_id + "' data-practice-name='" + data[i].practice_name + "' data-user-id='" + user_id + "' data-course-id='" + course_id + "'><i class='fa fa-chain-broken' aria-hidden='true' ></i> Eliminar Practica</button></td></tr>");
                    }
                }
            });
        });

    });
</script>