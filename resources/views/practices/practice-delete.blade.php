<!-- Modal -->
<div class="modal fade" id="deletePractice" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Eliminar Practica</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group row">
                        <label for="" class="col-sm-2">Tema:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="delete-practice-name" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="">Justificación</label>
                      <textarea class="form-control" name="justification" rows="3" id="justification"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-danger delete" data-dismiss="modal">Eliminar</button>
            </div>
        </div>
    </div>
</div>