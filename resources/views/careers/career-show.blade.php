@extends('layouts.app')

{{-- The menu items are set --}}
@section('section')
    Carreras
@endsection
@section('show')
    <li class="breadcrumb-item active">
       {{ $career->career_name }}
    </li>
@endsection

{{-- Creation of the table for show courses --}}
@section('content')
<div class="form-group">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Tabla de Cursos
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th colspan="8">
                                <a name="newCourse" id="" class="btn btn-outline-primary btn-block" href="{{ route('careers.courses.create', $career->career_id) }}" role="button">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Nuevo Curso
                                </a>
                            </th>
                        </tr>
                        <tr role="row" class="text-center">
                            <th colspan="4">Datos del Curso</th>
                            <th colspan="4">Acciones</th>
                        </tr>
                        <tr role="row">
                            <th>Carrera</th>
                            <th>Grupo</th>
                            <th>Asignatura</th>
                            <th>Periodo</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subjects as $subject)
                            <tr class="item{{ $loop->iteration }}">
                                <td>{{ $career->career_name }}</td>
                                <td>{{ $subject->group_id }}</td>
                                <td>{{ $subject->subjects->subject_name }}</td>
                                <td>{{ $subject->period_id }}</td>
                                <td>
                                    <a class="btn btn-outline-info" href="{{ route('courses.inscriptions.index', $subject->course_id) }}" role="button"><i class="fa fa-sign-in" aria-hidden="true"></i> Alumnos Inscritos</a>
                                </td>
                                <td class="text-center">
                                <a href="{{ route('courses.practices.index', $subject->course_id) }}" role="button" class="btn btn-outline-success">
                                        <i class="fa fa-flash" aria-hidden="true"></i> Gestionar 
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('careers.courses.edit',['career' => $career->career_id, 'course' => $subject->course_id]) }}" role="button" class="btn btn-outline-warning">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar
                                    <a>
                                </td>
                                <td class="text-center">
                                    <a role="button" href="#" class="btn btn-outline-danger nav-link delete-course" data-toggle="modal" data-target="#deleteCourse" data-course-id="{{ $subject->course_id }}" data-position="{{ $loop->iteration }}" data-user-id="{{ auth()->user()->user_id }}"><i class="fa fa-ban" aria-hidden="true"></i> Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Carrera</th>
                            <th>Grupo</th>
                            <th>Asignatura</th>
                            <th>Periodo</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@include('courses/modals.course-delete')

@push('crud')
    @include('courses.ajax.crud_js')
@endpush

@endsection