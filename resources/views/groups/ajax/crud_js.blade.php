@include('shared._root-route')
{{-- function for create a new group --}}
<script type="text/javascript">
    $(function () {  
        var $createGroup = $('#createGroup');
        var $errorGroup = $('.errorGroupId');
        var $group_id = $('#group_id');
        var $cancelBtn = $('.cancel');

        $createGroup.on('click', '.create', function () {
            $.ajax({
                type: 'POST',
                url: url + '/' + 'groups',
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'group_id': $group_id.val()
                },
                success: function (data) {
                    $errorGroup.addClass('hidden');

                    if ((data.errors)) {
                        setTimeout(function () {  
                            $('#createGroup').modal('show');
                            toastr.error('Error de Validación', 'Alerta de Error!', {timeOut: 3000});
                        }, 500);

                        if (data.errors.group_id) {
                            $errorGroup.removeClass('hidden');
                            $errorGroup.text(data.errors.group_id);
                        }
                    } else {
                        toastr.success('Se ha agregado un grupo', 'Registro Exitoso', {timeOut: 3000});
                    }
                },
            });
            $group_id.val('');
        });

        $cancelBtn.on('click', function () {  
            $group_id.val('');
        });

    });
</script>