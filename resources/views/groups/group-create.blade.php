<!-- Modal for delete a period -->
<div class="modal fade" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">Registrar un nuevo Grupo</h5>
                    <button class="close cancel" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <div class="form-group row">
                            <label for="" class="col-sm-2">Grupo:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="group_id" id="group_id" placeholder="1640">
                                <p class="errorGroupId text-center alert alert-danger hidden" role="alert"></p>
                            </div>
                        </div>
                    </form>
                    Selecciona Crear para guardar los cambios
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-success cancel" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-outline-info create" data-dismiss="modal">Crear</button>
                </div>
            </div>
        </div>
    </div>