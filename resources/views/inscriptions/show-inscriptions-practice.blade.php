@extends('layouts.app')

{{-- The menu items are set --}}
@section('section')
    Cursos
@endsection
@section('show')
    <li class="breadcrumb-item active">
        {{ $career->career_name }}
    </li>
@endsection
@section('option')
    <li class="breadcrumb-item active">
        Practicas
    </li>
@endsection
@section('data')
    <li class="breadcrumb-item active">
        {{ $subject->subject_name . '-' . $group->group_id . '-' . $period->period_id}}
    </li>
@endsection

@section('content')
    <div class="form-group">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Tabla de Alumnos Inscritos
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead >
                            <th>Nombre</th>
                            @foreach ($course_practices as $course)
                                @foreach ($course->registereds as $registered)
                                    @foreach ($registered->practices as $practice)
                                        <th>{{ $practice->practice_name . "\n" . date('d/m/Y', strtotime($practice->practice_date)) }}</th>
                                    @endforeach
                                @endforeach
                            @endforeach
                            <th>N°. de Practicas entregadas</th>
                            <th>Porcentaje</th>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($course_practices as $course)
                                @foreach ($course->inscriptions as $inscription)
                                    <tr>
                                        <td>
                                            {{ $inscription->students->user->personalInformation->fullName() }}
                                        </td>
                                        @forelse ($inscription->practices as $practice)
                                            <td>
                                                <input type="checkbox" class="form-check-input" checked>
                                            </td>
                                        @empty
                                            <td>
                                                <input type="checkbox" class="form-check-input">
                                            </td>
                                        @endforelse
                                        <td>
                                            <input type="text" class="form-control text-center" value="{{ count($inscription->practices) }}" disabled>
                                        </td>
                                        <td>
                                            @if ((bool)count($inscription->practices))
                                                <input type="text" class="form-control text-center" value="{{ $inscription->practices->sum('value') / count($inscription->practices) }}" disabled>
                                            @else
                                                <input type="text" class="form-control text-center" value="0" disabled>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        </tbody>
                        <tfoot>
                            <th>Nombre</th>
                            @foreach ($course_practices as $course)
                                @foreach ($course->registereds as $registered)
                                    @foreach ($registered->practices as $practice)
                                        <th>{{ $practice->practice_name . "\n" . date('d/m/Y', strtotime($practice->practice_date)) }}</th>
                                    @endforeach
                                @endforeach
                            @endforeach
                            <th>N°. de Practicas entregadas</th>
                            <th>Porcentaje</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection