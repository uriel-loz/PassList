<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bienvenido Usuario</title>
</head>
<body>
    <h1>Bienvenido</h1>
    <h3>Hola y bienvenido a PassList, gracias por crear una cuenta.</h3>
    <h3>Por favor verifica los datos de la cuenta:</h3>
    <p>
        Nombre: {{ $personalInformation->fullName() }} <br>
        email: {{ $personalInformation->email }}
    </p>
    <h3>Si los datos son correctos, por favor verifica tu correo dando click en el siguiente enlace: </h3>
    <a href="{{ route('verify', $user->verification_token) }}">Verificar</a>
</body>
</html>