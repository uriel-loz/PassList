@include('shared._head')
<body class="bg-dark">
    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Restaurar Contraseña</div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

          <div class="text-center mt-4 mb-5">
            <h4>¿Olvidaste tu contraseña?</h4>
            <p>Ingresa tu correo electronico y te enviaremos instrucciones para restaurar tu contraseña.</p>
          </div>
          <form method="POST" action="{{ route('password.email') }}">

            @csrf

            <div class="form-group">
              <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Ingresa tu correo electronico" name="email">
                @if ($errors->has('email'))
                    <span class="error">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-outline-danger btn-block">
                <i class="fa fa-unlock" aria-hidden="true"></i> 
                Restaurar Contraseña
            </button>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="{{ route('register') }}">Registrarte</a>
            <a class="d-block small" href="{{ route('login') }}">Iniciar Sesión</a>
          </div>
        </div>
      </div>
    </div>
@include('shared._scripts')
