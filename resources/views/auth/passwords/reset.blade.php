@include('shared._head')
    <body class="bg-dark">
        <div class="container">
            <div class="card card-login mx-auto mt-5">
                <div class="card-header">Restaurar Contraseña</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('password.request') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email">Dirección de correo electronico</label>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}"
                                required autofocus> 
                            @if ($errors->has('email'))
                                <span class="error">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required> 
                            @if ($errors->has('password'))
                                <span class="error">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">Verificar Contraseña</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Verificar Contraseña" required>
                        </div>

                        <button type="submit" class="btn btn-outline-danger btn-block">
                            <i class="fa fa-unlock" aria-hidden="true"></i>
                            Restaurar Contraseña
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </body>
@include('shared._scripts')