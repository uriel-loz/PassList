@include('shared._head')
<body class="bg-dark">
    <div class="container">
        <div class="card card-register mx-auto">
            <div class="card-header">Registrarse</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="" class="col-md-3">Numero de Cuenta</label>
                            <div class="col-md-6">
                                <input type="tel" class="form-control" placeholder="313938271" name="user_id" value="{{ old('user_id') }}" required>

                                @if ($errors->has('user_id'))
                                    <span class="error">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="exampleInputName">Nombre</label>
                                    <input type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="error">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleInputLastName">Apellido Paterno</label>
                                    <input class="form-control" type="text" aria-describedby="nameHelp" placeholder="Apellido Paterno" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="error">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-md-3">Apellido Materno</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" aria-describedby="nameHelp" placeholder="Apellido Materno" name="last_mother_name" value="{{ old('last_mother_name') }}" required autofocus>

                                @if ($errors->has('last_mother_name'))
                                    <span class="error">
                                        <strong>{{ $errors->first('last_mother_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input class="form-control" type="email" aria-describedby="emailHelp" placeholder="Email" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="error">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="">Telefono</label>
                                    <input type="tel" class="form-control" name="phone" placeholder="Telefono" value="{{ old('phone') }}" required>

                                    @if ($errors->has('phone'))
                                        <span class="error">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>  
                            </div>
                        </div>
                       <div class="form-group">
                           <label for="">Lista de carreras</label>
                           <select class="custom-select" name="career_id" id="">
                               <option selected>-- Selecciona tu carrera --</option>
                               @foreach ($careers as $career)
                                    <option value="{{ $career->career_id }}">{{ $career->career_name }}</option>
                               @endforeach
                           </select>

                           @if ($errors->has('career'))
                                <span class="error">
                                    <strong>{{ $errors->first('career') }}</strong>
                                </span>
                            @endif
                       </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="exampleInputPassword1">Contraseña</label>
                                    <input class="form-control password" type="password" placeholder="Contraseña" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="error">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleConfirmPassword">Verificar Contraseña</label>
                                    <input class="form-control password" type="password" placeholder="Verificar Contraseña" name="password_confirmation" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-row">
                                <div class="form-check offset-md-4 ">
                                    <input class="form-check-input" type="checkbox" id="show" value=""> Mostrar Contraseñas
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-12 col-md-2 offset-md-3 col-lg-2 offset-lg-2 col-form-label">Género:</label>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" value="M" name="gender" checked>
                                <label for="" class="form-check-label">Masculino</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" class="form-check-input" value="F" name="gender">
                                <label for="" class="form-check-label">Femenino</label>
                            </div>
                        </div>
                    <button type="submit" class="btn btn-outline-primary btn-block">
                        <i class="fa fa-btn fa-user"></i> Registrarme
                    </button>
                </form>
                <div class="text-center">
                    <a class="d-block small mt-3" href="{{ route('login') }}">Iniciar Sesión</a>
                    <a class="d-block small" href="{{ route('password.request') }}">¿Olvide mi Contraseña?</a>
                </div>
            </div>
        </div>
    </div>
@include('shared._scripts')