@include('shared._head')
<body class="bg-dark">

    @if(session('warning'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('warning') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if(session('success'))
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Iniciar Sesión</div>
                <div class="card-body">
                <form method="POST" action="{{ route('login') }}">

                    @csrf

                    <div class="form-group">
                        <label for="account">Numero de cuenta</label>
                        <input class="form-control" id="exampleInputEmail1" type="tel" aria-describedby="emailHelp" placeholder="315988931" name="user_id" value="{{ old('user_id') }}" required>

                        @if ($errors->has('user_id'))
                            <span class="error">
                                <strong>{{ $errors->first('user_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Contraseña</label>
                        <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Contraseña" name="password" required> 

                        @if ($errors->has('password'))
                            <span class="error">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="rename"> Recordar Contraseña
                            </label>
                        </div>
                    </div>
                    <button type="submit" value="login" class="btn btn-outline-primary btn-block"><i class="fa fa-sign-in" aria-hidden="true"></i> Entrar</button>
                    <div class="text-center">
                        <a class="d-block small mt-3" href="{{ route('register') }}" >Registrar una Cuenta</a>
                        <a class="d-block small" href="{{ route('password.request') }}" >¿Olvide mi Contraseña?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@include('shared._scripts')