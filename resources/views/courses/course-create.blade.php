@extends('layouts.app')

{{-- The submenu items are set --}}
@section('section')
    Carreras
@endsection
@section('show')
    <li class="breadcrumb-item active">
        {{ $career->career_name }}
    </li>
@endsection
@section('option')
    <li class="breadcrumb-item active">
        Nuevo Curso
    </li>
@endsection

{{-- Creation of the form for new courses create --}}
@section('content')
    <form method="POST" action="{{ route('careers.courses.store', $career->career_id) }}">  
        @csrf

        <fieldset disabled>
            <div class="form-group row">
                <label for="disabledTextInput" class="col-sm-1 col-md-1 offset-md-3">Carrera:</label>
                <div class="col-sm-11 col-md-5">
                    <input class="form-control text-center" type="text"  value="{{ $career->career_name }}">
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <label for="" class="col-sm-1 col-md-1 offset-md-3">Asignatura:</label>
            <div class="col-sm-11 col-md-5 col-lg-5">
                <select name="subject_id" class="custom-select" required>
                    <option value="">-- Seleccionar un Asignatura --</option>
                    @foreach ($subjects as $subject)
                        <option value="{{ $subject->subject_id }}">{{ $subject->subject_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-1 col-md-1 offset-md-3">Periodo:</label>
            <div class="col-sm-11 col-md-5 col-lg-5">
                <select name="period_id" class="custom-select" required>
                    <option value="">--Seleccionar un Periodo--</option>
                    @foreach ($periods as $period)
                        <option value="{{ $period->period_id }}">{{ $period->period_id }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-1 col-md-1 offset-md-3">Grupo:</label>
            <div class="col-sm-11 col-md-5">
                <input class="form-control text-center" type="text" placeholder="1101" name="group_id" value="{{ old('group_id') }}">
                {!! $errors->first('group_id', '<span class=error>:message</span>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="form-row">
                <div class="col-sm-12 col-md-2">
                    <a class="btn btn-outline-info col-sm-12 nav-link" href="#" role="button" data-toggle="modal" data-target="#createGroup"><i class="fa fa-gg-circle" aria-hidden="true"></i> Crear Grupo</a>
                </div>
                <div class=" col-sm-12 col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-outline-success col-sm-12 col-md-3 " value="send"><i class="fa fa-flash" aria-hidden="true"></i> Registrar</button>
                    <a class="btn btn-outline-danger col-sm-12 col-md-3" href="{{ route('careers.courses.index', $career->career_id) }}" role="button"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                </div>
            </div>
        </div>
    </form>

    @include('groups.group-create')

    @push('crud')
        @include('groups.ajax.crud_js')
    @endpush

@endsection