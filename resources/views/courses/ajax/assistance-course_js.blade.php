@include('shared._root-route')

<script type="text/javascript">
    $(function () {  
        'use strict';
        var $newAssistance = $('#newAssistance'); //buttonAdd
        var $removeAssistance = $('#removeAssistance'); //buttonRemove
        var $attendanceDate = $('.attendanceDate');//thead and tfooter
        var $checkAssistance = $('.checkAssistance'); //tbody

        var date = new Date();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var year = date.getFullYear();

        var fullDate = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + year;

        $newAssistance.on('click', function () {
            $attendanceDate.after($('<th>', {
                'text' : 'Asistencia' + "\n" + fullDate
            }));
            
            $checkAssistance.after($('<td>').append($('<div>', {
                'class' : 'form-check'
            }).append($('<input>', {
                'class': 'form-check-input assistance',
                'type': 'checkbox',
                'value': '1',
                'data-assistance-date': fullDate,
            }))));
        });
        //funcion to add new assistance

        $removeAssistance.on('click', function () {
            $attendanceDate.next().remove();
            $checkAssistance.next().remove();
        });
        // functionality to erase assists
        
        //function for create new assitance
        $(document).on('click', '.assistance', function () {  
            var $position = $('.assistance').index(this); 
            var checkbox = document.getElementsByClassName('assistance');
            var inscription_id = checkbox[$position].parentElement.parentElement.parentElement.getAttribute('data-inscription-id');
            var value = checkbox[$position].getAttribute('value');
            var date_assistance = checkbox[$position].getAttribute('data-assistance-date');

            $.ajax({
                type: 'POST',
                url: url + '/' + 'assistances',
                data: {
                    '_token' : '{!! csrf_token() !!}',
                    'assistance_date': convertDateFormatBackend(date_assistance),
                    'assistance': value,
                    'inscription_id': inscription_id
                },
                success: function (results) {  
                    console.log(results);
                },
            }); 
        });
    });
</script>