@include('shared._root-route')

<script type="text/javascript">
    $(function () {  
        'use strict';
        var $studentData = $('.student-data'); //variable where get data
        var $studentName = $('#student-name'); //variable where show student name
        var $inscriptionInput = $('#head');//variable find place where attach the input
        var $addPractice = $('.add-practice');
        var $cancel = $('.cancel')

        $studentData.on('click', function () {  
            $studentName.val($(this).data('student-name'));
            var $position = $('.student-data').index(this);
            var $student = $studentData[$position];
            var inscription_id = $student.parentElement.parentElement.getAttribute('data-inscription-id');
            $inscriptionInput.append($('<input>',{
                'type': 'text',
                'id': 'inscription_id',
                'class': 'hidden',
                'value': inscription_id
            }));
        });

        $addPractice.on('click', function () {  
           var $practice_id = $('#practice_id');
           var $inscription_id = $('#inscription_id');
           var $practice_date = $studentData.data('practice-date');
           $.ajax({
               type: 'GET',
               url: url + '/' + 'inscriptions' + '/' + $inscription_id.val(),
               data: {
                   'practice_id': $practice_id.val(),
                   'practice_date': $practice_date
               },
               success: function () {  
                   toastr.success('Se ha registrado la practica al alumno', 'Registro de Nueva Practica Exitoso!', {timeOut: 4000});
               },
           });

            $inscription_id.remove();
        });

        $cancel.on('click', function () {  
            var $inscription_id = $('#inscription_id');
            $inscription_id.remove();
        });
    });
</script>