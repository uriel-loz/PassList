@include('shared._root-route')

{{-- function to delete a course  --}}
<script type="text/javascript">
    $(function () {  
        // function to pass parameters to the modal
        $(document).on('click', '.delete-course', function () {  
            var $course_id = $(this).data('course-id');
            var $user_id = $(this).data('user-id');
            var $position_id = $(this).data('position');

            course_id = $course_id;
            user_id = $user_id;
            position = $position_id;   
        });

        $('#deleteCourse').on('click', '.delete', function () {  
            $.ajax({
                type: 'DELETE',
                url: url + '/' + 'courses' + '/' + course_id + '/' + 'teachers' + '/' + user_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                },
                success: function (data) {  
                    toastr.success('Se ha eliminado un curso', 'Eliminación Exitosa!', {timeOut: 3000});
                    $('.item' + position).remove();
                },
            });
        });
    });
</script>