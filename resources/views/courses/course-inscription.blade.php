@extends('layouts.app')

{{-- The menu items are set --}}
@section('section')
    Cursos
@endsection
@section('show')
    <li class="breadcrumb-item active">
        {{ $career->career_name }}
    </li>
@endsection
@section('option')
    <li class="breadcrumb-item active">
        Inscripciones
    </li>
@endsection
@section('data')
    <li class="breadcrumb-item active">
        {{ $subject->subject_name . '-' . $group->group_id . '-' . $period->period_id}}
    </li>
@endsection

@section('content')
    <div class="form-group">
        <div class="form-row">
            <button type="button" class="btn btn-outline-success col-6" id="newAssistance"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva Asistencia</button>
            <button type="button" class="btn btn-outline-danger col-6" id="removeAssistance"><i class="fa fa-minus-circle" aria-hidden="true"></i> Eliminar Asistencia</button>
        </div>
    </div> 
    <div class="form-group">
        <div class="form-row">
            <a  role="button" href="{{ route('courses.inscriptions.practices.show', $course->course_id) }}" class="btn btn-outline-info col-12" id="recordPractice"><i class="fa fa-address-book-o" aria-hidden="true"></i> Registro de Practicas de los alumnos</a>
        </div>
    </div>     

    <div class="form-group">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Tabla de Alumnos Inscritos
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead >
                            <tr>
                                <th class="attendanceDate">Nombre</th>
                                @foreach ($assistance_dates as $date)
                                    @if (count($assistance_dates) > 0)
                                        <th>{{ 'Asistencia ' . date('d/m/Y', strtotime($date->assistance_date))}}</th>
                                    @endif
                                @endforeach
                                <th>Total de <br> Asistencias</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($assistance_student as $student)
                                <tr data-inscription-id="{{ $student->inscription_id }}">
                                    <td class="checkAssistance">
                                        <a class="nav-link pass-assign student-data" href="#" data-toggle="modal" data-target="#practiceRecord" data-student-name="{{ $student->students->user->personalInformation->fullName() }}" data-practice-date="{{ date('Y-m-d') }}">{{ $student->students->user->personalInformation->fullName() }}</a>
                                    </td>
                                    @forelse ($student->assistances as $assistance)
                                        <td>
                                            <input type="checkbox" class="form-check-input assistance"  {{ $assistance->assistance ? 'checked value="1"' :  '' }}>
                                        </td>
                                    @empty
                                        @if (count($assistance_dates) > 0)
                                            <td>
                                                <input type="checkbox" class="form-check-input assistance" value="0">
                                            </td>
                                        @endif
                                    @endforelse
                                    <td>
                                        <input type="text" class="form-control text-center" {{ 'value= ' . count($student->assistances->where('assistance', '1'))  }} disabled>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="attendanceDate">Nombre</th>
                                @foreach ($assistance_dates as $date)
                                    @if (count($assistance_dates) > 0)
                                        <th>{{ 'Asistencia ' . date('d/m/Y', strtotime($date->assistance_date))}}</th>
                                    @endif
                                @endforeach
                                <th>Total de <br> Asistencias</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <a class="btn btn-outline-primary col-12" href="{{ route('careers.courses.index', $career->career_id) }}" role="button"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Volver</a>
    </div>

    @include('courses.modals.course-practice')

    @push('crud')
        @include('courses.ajax.assistance-course_js')
    @endpush

    @push('crud')
        @include('courses.ajax.practice-record-course_js')
    @endpush

@endsection