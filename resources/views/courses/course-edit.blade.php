@extends('layouts.app')

{{-- The submenu items are set --}}
@section('section')
    Carreras
@endsection
@section('show')
    <li class="breadcrumb-item active">
        {{ $career->career_name }}
    </li>    
@endsection
@section('option')
    <li class="breadcrumb-item active">
        {{ 'Editar Curso'. ' - ' . $subject->subject_name }} 
    </li>
@endsection

{{-- Creation of the form for new courses create --}}
@section('content')
    <form method="POST" action="{{ route('careers.courses.update', ['career' => $career->career_id, 'course' => $course->course_id]) }}">  
        @csrf
        {!! method_field('PUT') !!}
        <fieldset disabled>
            <div class="form-group row">
                <label for="disabledTextInput" class="col-sm-1 col-md-1 offset-md-3">Carrera:</label>
                <div class="col-sm-11 col-md-4">
                    <input class="form-control text-center" type="text"  value="{{ $career->career_name }}" name="career">
                </div>
            </div>
        </fieldset>
        <div class="form-group row">
            <label for="" class="col-sm-1 col-md-1 offset-md-3">Periodo:</label>
            <div class="col-sm-11 col-md-4 col-lg-4">
                <select name="period_id" class="custom-select" required>
                    <option value="">-- Seleccionar un Periodo --</option>
                    @foreach ($periods as $period)
                        <option value="{{ $period->period_id }}">{{ $period->period_id }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="" class="col-sm-1 col-md-1 offset-md-3">Grupo:</label>
            <div class="col-sm-11 col-md-4">
                <input class="form-control text-center" type="text" value="{{ $course->group_id }}" name="group_id">
                {!! $errors->first('group_id', '<span class=error>:message</span>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="offset-3"><label for="" >Justificación:</label>
                <textarea name="justification" rows="4" class="form-control col-sm-7"></textarea>
                {!! $errors->first('justification', '<span class=error>:message</span>') !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-md-4 offset-lg-9">
                <button type="submit" class="btn btn-outline-info"><i class="fa fa-pencil-square-o" aria-hidden="true"> </i>Actualizar</button>
                <a class="btn btn-outline-danger" href="{{ route('careers.courses.index', $career->career_id) }}" role="button"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
            </div>
        </div>
    </form>
@endsection