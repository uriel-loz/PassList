@extends('layouts.app')

{{-- The submenu items are set --}}
@section('section')
    Carreras
@endsection
@section('show')
    <li class="breadcrumb-item active">
        {{ $career->career_name }}
    </li>
@endsection
@section('option')
    <li class="breadcrumb-item active">
        Gestionar Curso
    </li>
@endsection
@section('data')
    <li class="breadcrumb-item active">
        {{ $career->career_name . ' - ' . $subject->subject_name . ' - ' . $course->group_id . ' - '  . $course->period_id }}
    </li>
@endsection

@section('content')
<div class="form-group">
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Tabla de Practicas
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th colspan="7">
                                <button type="button" class="btn btn-outline-primary btn-block nav-link btn-create-practice" data-toggle="modal" data-target="#createPractice" data-course-id="{{ $course->course_id }}" data-user-id="{{ auth()->user()->user_id }}" data-career-name="{{ $career->career_name }}">
                                    <i class="fa fa-file-o" aria-hidden="true" ></i> Nueva Practica
                                </button>
                            </th>
                        </tr>
                        <tr>
                            <th colspan="5" class="text-center">Datos de practica</th>
                            <th colspan="2" class="text-center">Opciones</th>
                        </tr>
                        <tr class="text-center">
                            <th>Tema:</th>
                            <th>Descripción</th>
                            <th>Valor</th>
                            <th>Fecha de entrega</th>
                            <th></th>
                            <th></th>
                        </tr>
                        {{ csrf_field() }}
                    </thead>
                    <tbody class="text-center" id="table-body">
                        @foreach ($practices as $practice)
                            <tr>
                                <td>{{ $practice->practice_name }}</td>
                                <td>{{ $practice->description }}</td>
                                <td>{{ $practice->value }}</td>
                                <td>{{ date('d/m/Y', strtotime($practice->practice_date)) }}</td>
                                <td>
                                    <button type="button" class="btn btn-outline-info nav-link btn-edit-practice" data-toggle="modal" data-target="#editPractice" data-practice-id="{{ $practice->practice_id }}" data-practice-name="{{ $practice->practice_name }}" data-practice-description="{{ $practice->description }}" data-practice-value="{{ $practice->value }}" data-practice-date="{{ $practice->practice_date }}" data-course-id="{{ $course->course_id }}" data-user-id="{{ auth()->user()->user_id }}">
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i> Editar Practica
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-danger nav-link btn-delete-practice" data-toggle="modal" data-target="#deletePractice" data-practice-id="{{ $practice->practice_id }}" data-practice-name="{{ $practice->practice_name }}" data-user-id="{{ auth()->user()->user_id }}" data-course-id="{{ $course->course_id }}">
                                        <i class="fa fa-chain-broken" aria-hidden="true" ></i> Eliminar Practica
                                    </button>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr class="text-center">
                            <th>Tema</th>
                            <th>Descripción</th>
                            <th>Valor</th>
                            <th>Fecha de entrega</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="form-row">
    <a role="button" class="btn btn-outline-danger btn-block" href="{{ route('careers.courses.index', $career->career_id) }}"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Regresar</a>
</div>

@include('practices/practice-create')
@include('practices/practice-edit')
@include('practices/practice-delete')

@push('crud')
    @include('practices.ajax.crud_js')
@endpush

@endsection