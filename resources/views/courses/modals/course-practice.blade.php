<!-- Modal for add an practice -->
<div class="modal fade" id="practiceRecord" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Practicas del Alumno</h5>
                <button class="close cancel" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group row" id="head">
                        <input type="text" class="form-control col-12 text-center" id="student-name" disabled>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                          <select class="form-control" name="practice_id" id="practice_id">
                            <option>-- Selecciona la practica a entregar --</option>
                            @foreach ($course_practices as $practice)
                                <option value="{{ $practice->practice_id }}">{{ $practice->practice_name }}</option>
                            @endforeach
                          </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success cancel" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-danger add-practice" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>