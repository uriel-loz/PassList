<!-- Modal for delete a course -->
<div class="modal fade" id="deleteCourse" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">¿Estas seguro de eliminar este curso?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="" >Justificación:</label>
                        <textarea name="justification" rows="4" class="form-control"></textarea>
                    </div>
                </form>
                Selecciona Eliminar para borrar el curso.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-danger delete" data-dismiss="modal">Eliminar</button>
            </div>
        </div>
    </div>
</div>