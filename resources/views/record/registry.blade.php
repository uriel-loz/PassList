@extends('layouts.app')
@section('section')
    Record
@endsection

@section('content')
    <div class="form-group">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Tabla del Historial
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Usuario afectado</th>
                                <th>Tipo de movimiento</th>
                                <th>Justificación</th>
                                <th>Hora y fecha</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>311282920</td>
                                <td>412029201</td>
                                <td>Peticion denegada</td>
                                <td>
                                    <button type="button" class="btn btn-outline-info nav-link" data-toggle="modal" data-target="#description"><i class="fa fa-info" aria-hidden="true"></i> Detalles</button>
                                </td>
                                <td>29/05/18 18:54</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Usuario</th>
                                <th>Usuario afectado</th>
                                <th>Tipo de movimiento</th>
                                <th>Justificación</th>
                                <th>Hora y fecha</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@include('record/modals.description-movement')    
@endsection