<!-- Modal for delete a period -->
<div class="modal fade" id="deletePeriod" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">¿Realmente quieres eliminar este periodo?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group row">
                        <label for="" class="col-sm-2">Periodo:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="period_id" id="period_id_delete" disabled>
                        </div>
                    </div>
                </form>
                Selecciona Eliminar para guardar los cambios
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-danger delete" data-dismiss="modal">Eliminar</button>
            </div>
        </div>
    </div>
</div>