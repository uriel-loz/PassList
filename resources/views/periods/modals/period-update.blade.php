<!-- Modal for update a period -->
<div class="modal fade" id="updatePeriod" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Actualizar información del periodo</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group row">
                        <label for="" class="col-sm-2">Periodo:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="period_id" id="period_id_edit" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Fecha de Inicio:</label>
                            <input type="date" class="form-control" name="period_start" id="period_start_date_edit" step="0.01">
                            <p class="errorPeriodStart text-center alert alert-danger hidden" role="alert"></p>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="">Fecha de Termino:</label>
                            <input type="date" class="form-control" name="period_end" id="period_end_date_edit" step="0.01">
                            <p class="errorPeriodEnd text-center alert alert-danger hidden" role="alert"></p>
                        </div>
                    </div>
                </form>
                Selecciona Actualizar para guardar los cambios
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-info edit" data-dismiss="modal">Actualizar</button>
            </div>
        </div>
    </div>
</div>