<!-- Modal for create a period -->
<div class="modal fade" id="createPeriod" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Crear un nuevo periodo</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group row">
                        <label for="" class="col-sm-2">Periodo:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="period_id" name="period_id">
                            <p class="errorPeriodId text-center alert alert-danger hidden" role="alert"></p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Fecha de Inicio:</label>
                            <input type="date" class="form-control" id="period_start_date" name="period_start_date" step="0.01">
                            <p class="errorPeriodStart text-center alert alert-danger hidden" role="alert"></p>
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="">Fecha de Termino:</label>
                            <input type="date" class="form-control" id="period_end_date" name="period_end_date" step="0.01">
                            <p class="errorPeriodEnd text-center alert alert-danger hidden" role="alert"></p>
                        </div>
                    </div>
                </form>
                Selecciona Crear para guardar los cambios
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-success" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline-info create-period" data-dismiss="modal">Crear</button>
            </div>
        </div>
    </div>
</div>