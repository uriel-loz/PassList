@extends('layouts.app')

@section('section')
    Periodos
@endsection

@section('content')
    <div class="form-group">
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Tabla de Periodos
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th colspan="7">
                                    <a class="btn btn-outline-primary btn-block nav-link" data-toggle="modal" href="#" role="button" data-target="#createPeriod">
                                        <i class="fa fa-plus" aria-hidden="true"></i> Nuevo Periodo
                                    </a>
                                </th>
                            </tr>
                            <tr role="row" class="text-center">
                                <th colspan="3">Datos del Periodo</th>
                                <th colspan="2">Acciones</th>
                            </tr>
                            <tr role="row">
                                <th>Periodo</th>
                                <th>Fecha de Inicio</th>
                                <th>Fecha de termino</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($periods as $period)
                                <tr class="text-center item{{ $period->period_id }}" >
                                    <td>{{ $period->period_id }}</td>
                                    <td>{{ date('d/m/Y', strtotime($period->period_start_date)) }}</td>
                                    <td>{{ date('d/m/Y', strtotime($period->period_end_date)) }}</td>
                                    <td class="text-center">
                                        <a role="button" href="#" class="btn btn-outline-success nav-link edit-period" data-toggle="modal" data-target="#updatePeriod" data-period-id="{{ $period->period_id }}" data-period-start="{{ $period->period_start_date }}" data-period-end="{{ $period->period_end_date }}"><i class="fa fa-refresh" aria-hidden="true"></i> Actualizar</a>
                                    </td>
                                    <td class="text-center">
                                        <a role="button" class="btn btn-outline-danger nav-link delete-period" href="#" data-toggle="modal" data-target="#deletePeriod" data-period-id="{{ $period->period_id }}"><i class="fa fa-ban" aria-hidden="true"></i> Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Periodo</th>
                                <th>Fecha de Inicio</th>
                                <th>Fecha de termino</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('periods.modals.period-create')
    @include('periods.modals.period-update')
    @include('periods.modals.period-delete')

    @push('crud')
        @include('periods.ajax.crud_js')
    @endpush

@endsection