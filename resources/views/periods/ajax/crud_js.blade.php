@include('shared._root-route')

{{-- Ajax for crud functions --}}
<script type="text/javascript">
    $(function () {  
        // module for create to period
       $('#createPeriod').on('click', '.create-period', function () {
           $.ajax({
               type: 'POST',
               url: url + '/' + 'periods',
               data: {
                   '_token': '{!! csrf_token() !!}',
                   'period_id': $('#period_id').val(),
                   'period_start_date': $('#period_start_date').val(),
                   'period_end_date': $('#period_end_date').val()
               },
               success : function (data) {  
                   $('.errorPeriodId').addClass('hidden');
                   $('.errorPeriodStart').addClass('hidden');
                   $('.errorPeriodEnd').addClass('hidden');

                   if ((data.errors)) {
                       setTimeout(function () {
                           $('#createPeriod').modal('show');
                           toastr.error('Error de Validación', 'Alerta de Error!', {timeOut: 3000});
                        }, 500);

                        if (data.errors.period_id) {
                            $('.errorPeriodId').removeClass('hidden');
                            $('.errorPeriodId').text(data.errors.period_id);
                        }

                        if (data.errors.period_start_date) {
                            $('.errorPeriodStart').removeClass('hidden');
                            $('.errorPeriodStart').text(data.errors.period_start_date);
                        }

                        if (data.errors.period_end_date) {
                            $('.errorPeriodEnd').removeClass('hidden');
                            $('.errorPeriodEnd').text(data.errors.period_end_date);
                        }
                   } else {
                       toastr.success('Se ha agregado un periodo', 'Registro Exitoso', {timeOut: 3000});
                       $('#dataTable').append("<tr class='text-center item" + data.period_id +"'><td>" + data.period_id + "</td><td>" + convertDateFormat(data.period_start_date) + "</td><td>" + convertDateFormat(data.period_end_date) + "</td><td class='text-center'><a role='button' href='#' class='btn btn-outline-success nav-link edit-period' data-toggle='modal' data-target='#updatePeriod' data-period-id='" + data.period_id + "' data-period-start='" + data.period_start_date + "' data-period-end='" + data.period_end_date + "'><i class='fa fa-refresh' aria-hidden='true'></i> Actualizar</a></td><td class='text-center'><a role='button' class='btn btn-outline-danger nav-link delete-period' href='#' data-toggle='modal' data-target='#deletePeriod' data-period-id='" + data.period_id + "'><i class='fa fa-ban' aria-hidden='true'></i> Eliminar</a></td></tr>");
                   }
               },
           });
        });

        // function to pass the parameters to the modal
        $(document).on('click', '.edit-period', function () {  
            $('#period_id_edit').val($(this).data('period-id'));
            $('#period_start_date_edit').val($(this).data('period-start'));
            $('#period_end_date_edit').val($(this).data('period-end'));
            period_id = $('#period_id_edit').val();
        });

        // module for update to period
        $('#updatePeriod').on('click', '.edit', function () {  
            $.ajax({
                type: 'PUT',
                url: url + '/' + 'periods' + '/' + period_id,
                data: {
                    '_token': '{!! csrf_token() !!}',
                    'period_id': $('#period_id_edit').val(),
                    'period_start_date': $('#period_start_date_edit').val(),
                    'period_end_date': $('#period_end_date_edit').val()
                },
                success: function (data) {
                    $('.errorPeriodStart').addClass('hidden');
                    $('.errorPeriodEnd').addClass('hidden');

                    if ((data.errors)) {
                       setTimeout(function () {  
                           $('#updatePeriod').modal('show');
                           toastr.error('Error de validación', 'Alerta de Error!', {timeOut: 3000});
                       }, 500);

                        if (data.errors.period_start_date) {
                            $('.errorPeriodStart').removeClass('hidden');
                            $('.errorPeriodStart').text(data.errors.period_start_date);   
                        }

                        if (data.errors.period_end_date) {
                            $('.errorPeriodEnd').removeClass('hidden');
                            $('.errorPeriodEnd').text(data.errors.period_end_date);
                        }
                    } else {
                        toastr.success('Datos actualizados', 'Registro Exitoso', {timeOut: 3000});
                        $('.item' + data.period_id).replaceWith("<tr class='text-center item" + data.period_id +"'><td>" + data.period_id + "</td><td>" + convertDateFormat(data.period_start_date) + "</td><td>" + convertDateFormat(data.period_end_date) + "</td><td class='text-center'><a role='button' href='#' class='btn btn-outline-success nav-link edit-period' data-toggle='modal' data-target='#updatePeriod' data-period-id='" + data.period_id + "' data-period-start='" + data.period_start_date + "' data-period-end='" + data.period_end_date + "'><i class='fa fa-refresh' aria-hidden='true'></i> Actualizar</a></td><td class='text-center'><a role='button' class='btn btn-outline-danger nav-link delete-period' href='#' data-toggle='modal' data-target='#deletePeriod' data-period-id='" + data.period_id + "'><i class='fa fa-ban' aria-hidden='true'></i> Eliminar</a></td></tr>");
                    }
                },
            });
        });

        //function to pass the parameter period_id to the modal
        $(document).on('click', '.delete-period', function () {  
            $('#period_id_delete').val($(this).data('period-id'));
            period_id = $('#period_id_delete').val();
        });
        
        // module for delete to period
        $('#deletePeriod').on('click', '.delete', function () {  
            $.ajax({
                type: 'DELETE',
                url: url + '/' + 'periods' + '/' + period_id,
                data: {
                    '_token': '{!! csrf_token() !!}'
                },
                success: function (data) {
                    toastr.success('Se ha eliminado un periodo', 'Eliminación Exitosa!', {timeOut: 3000});
                    $('.item' + data['period_id']).remove();
                }
            });
        });
    });
</script>