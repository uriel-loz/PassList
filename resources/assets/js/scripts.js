var $show = $('#show');
var $password = $('.password');

$show.change(function () {
    if ($show.is(':checked')) {
        $password.attr('type', 'text');
    } else {
        $password.attr('type', 'password');
    }
});
// function for show passwords

var $table = $('#dataTable');

$('document').ready(function () {
    $table.DataTable();
});
// function that allows to use DataTable Pluggin of jQuery

var $careers = $('#careers');
var $careersList = $('#careers-list');
var $subjects = $('#subjects');
var $subjectsList = $('#subjects-list')

$careers.on('click', function () {
    $careersList.addClass('scroll');
});

$careers.on('dblclick', function () {  
    $careersList.removeClass('scroll')
} );
//Adding and remove scrollbar in careers list 

var $loginEffect = $('.login_effect');

$loginEffect.on({
    'mouseover' : function () {
        $(this).attr('id', 'login');
    },
    'mouseleave' : function () {  
        $(this).removeAttr('id', 'login');
    }
});
//Effect for user name

function convertDateFormat(format) {
    var date = format.split('-').reverse().join('/');
    return date;
}
//function for change format date

function convertDateFormatBackend(format) {
    var date = format.split('/').reverse().join('-');
    return date;
}
//function for change format date for backend